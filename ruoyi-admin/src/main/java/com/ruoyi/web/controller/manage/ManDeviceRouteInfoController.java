package com.ruoyi.web.controller.manage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

import com.ruoyi.common.exception.UtilException;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.manage.domain.ManDeviceRouteInfo;
import com.ruoyi.manage.service.IManDeviceRouteInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 设备路由信息Controller
 * 
 * @author LR
 * @date 2024-03-26
 */
@Controller
@RequestMapping("/manage/routeInfo")
public class ManDeviceRouteInfoController extends BaseController
{

    @Autowired
    private IManDeviceRouteInfoService manDeviceRouteInfoService;

    /*****************************************   OLT设备  START   ***************************************/

    private String prefixOlt = "manage/routeInfoOlt";

    @RequiresPermissions("manage:routeInfoOlt:view")
    @GetMapping("/routeInfoOlt")
    public String routeInfoOlt()
    {
        return prefixOlt + "/routeInfoOlt";
    }

    /**
     * 查询OLT设备路由信息列表
     */
    @RequiresPermissions("manage:routeInfoOlt:list")
    @PostMapping("/listOlt")
    @ResponseBody
    public TableDataInfo listOlt(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        startPage();
        List<ManDeviceRouteInfo> list = manDeviceRouteInfoService.selectManDeviceRouteInfoListOlt(manDeviceRouteInfo);
        return getDataTable(list);
    }

    /**
     * 导出设备路由信息列表
     */
    @RequiresPermissions("manage:routeInfoOlt:export")
    @Log(title = "设备路由信息", businessType = BusinessType.EXPORT)
    @RequestMapping("/exportOlt")
    @ResponseBody
    public void exportOlt(ManDeviceRouteInfo manDeviceRouteInfo,HttpServletRequest request, HttpServletResponse response)throws Exception {
        List<ManDeviceRouteInfo> list = manDeviceRouteInfoService.selectManDeviceRouteInfoListOlt(manDeviceRouteInfo);
        if (list.size()==0) {
            throw new Exception("查询数据为空，无法导出");
        }
        String[] headers = new String[]{
                "区县", "是否精品", "备注", "设备名称","设备IP","单播外层vlan","单播内层vlan","光模块(km)",
                "BAS IP", "BAS绑定端口", "电路属性", "聚合端口","物理端口","链路带宽","对端设备名称","对端网管名称",
                "对端设备IP", "对端聚合端口", "对端物理端口"
        };
        Integer[] widths = new Integer[]{
                6, 4, 10, 20, 12, 7, 10, 6,
                11, 7, 13, 12, 8, 5, 23, 18,
                14, 12, 26
        };
        Map<String,Object> map = builderAttentionDataMap(list,headers,widths);
        exportExcel(map,request,response);
    }

    private Map<String, Object> builderAttentionDataMap(List<ManDeviceRouteInfo> list, String[] headers, Integer[] widths) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        String[][] rows = new String[list.size()][headers.length];
        for (int i = 0; i < list.size(); i++) {
            ManDeviceRouteInfo info =  list.get(i);
            rows[i][0] = info.getDeviceCounty() == null ? "" : info.getDeviceCounty() ;
            rows[i][1] = info.getIsGood() == null ? "" : info.getIsGood() ;
            rows[i][2] = info.getRemark() == null ? "" : info.getRemark() ;
            rows[i][3] = info.getDeviceName() == null ? "" : info.getDeviceName() ;
            rows[i][4] = info.getDeviceIp() == null ? "" : info.getDeviceIp() ;
            rows[i][5] = info.getOutsideVlan() == null ? "" : info.getOutsideVlan() ;
            rows[i][6] = info.getInnerVlan() == null ? "" : info.getInnerVlan() ;
            rows[i][7] = info.getOpticalModule() == null ? "" : info.getOpticalModule() ;
            rows[i][8] = info.getBasIp() == null ? "" : info.getBasIp() ;
            rows[i][9] = info.getBasPort() == null ? "" : info.getBasPort() ;
            rows[i][10] = info.getCircuitProperties() == null ? "" : info.getCircuitProperties() ;
            rows[i][11] = info.getClosePort() == null ? "" : info.getClosePort() ;
            rows[i][12] = info.getPhysicsPort() == null ? "" : info.getPhysicsPort() ;
            rows[i][13] = info.getLinkBandwidth() == null ? "" : info.getLinkBandwidth() ;
            rows[i][14] = info.getOppositeDeviceName() == null ? "" : info.getOppositeDeviceName() ;
            rows[i][15] = info.getOppositeNetworkName() == null ? "" : info.getOppositeNetworkName() ;
            rows[i][16] = info.getOppositeDeviceIp() == null ? "" : info.getOppositeDeviceIp() ;
            rows[i][17] = info.getOpppsiteClosePort() == null ? "" : info.getOpppsiteClosePort() ;
            rows[i][18] = info.getOppositePhysicsPort() == null ? "" : info.getOppositePhysicsPort() ;
        }
        dataMap.put("headers", headers);
        dataMap.put("rows", rows);
        dataMap.put("widths", widths);
        return dataMap;
    }

    /**
     * 对list数据源将其里面的数据导入到excel表单
     *
     * @return 结果
     */
    public void exportExcel(Map<String,Object> dataMap, HttpServletRequest
    request, HttpServletResponse response) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String filename = df.format(new Date())+"OLT路由信息表.xlsx";

        String agent = request.getHeader("USER-AGENT");
        if (agent != null && agent.toLowerCase().indexOf("firefox") > 0) {
            filename = "=?UTF-8?B?" + (new String(Base64Utils.encodeToString(filename.getBytes("UTF-8")))) + "?=";
        } else {
            filename = URLEncoder.encode(filename, "UTF-8");
        }


        response.setContentType("application/x-msdownload");
        response.setHeader("Content-Disposition", "attachment;filename=" + filename);
//		response.setHeader("Content-Disposition", "attachment; fileName="
//				+ URLEncoder.encode(filename , "UTF-8"));

//        if (dataMap.isEmpty()) {
//            return AjaxResult.error("数据为空");
//        }

        XSSFWorkbook workbook = new XSSFWorkbook();

        CellStyle borderStyle = workbook.createCellStyle();
        borderStyle.setBorderTop(BorderStyle.THIN);
        borderStyle.setBorderBottom(BorderStyle.THIN);
        borderStyle.setBorderLeft(BorderStyle.THIN);
        borderStyle.setBorderRight(BorderStyle.THIN);
        borderStyle.setWrapText(true);
        borderStyle.setAlignment(HorizontalAlignment.CENTER);
        borderStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        String[] headers = (String[]) dataMap.get("headers");
        Integer[] widths = (Integer[]) dataMap.get("widths");
        String[][] rows = (String[][]) dataMap.get("rows");
        XSSFSheet sheet = workbook.createSheet("OLT路由信息表");
        XSSFRow row = null;
        XSSFCell cell = null;
        XSSFCellStyle setBorder = workbook.createCellStyle();
        setBorder.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());// 设置背景色
        setBorder.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        setBorder.setBorderTop(BorderStyle.THIN);
        setBorder.setBorderBottom(BorderStyle.THIN);
        setBorder.setBorderLeft(BorderStyle.THIN);
        setBorder.setBorderRight(BorderStyle.THIN);
        setBorder.setWrapText(true);
        setBorder.setAlignment(HorizontalAlignment.CENTER);
        setBorder.setVerticalAlignment(VerticalAlignment.CENTER);
        int count = 10;
        if (headers != null) {
            count = headers.length;
            if (headers != null && count > 0) {
                row = sheet.createRow(0);
                for (int i = 0; i < count; i++) {
                    sheet.setColumnWidth(i, widths[i]*256);
                    cell = row.createCell((short) i);
                    String desc = headers[i];
                    cell.setCellValue(new XSSFRichTextString(desc));
                    cell.setCellStyle(setBorder);
                }
            }
        }
        if (rows != null) {
            int len = rows.length;
            int pointer = 1;
            if (rows != null && len > 0) {
                for (int i = 0; i < len; i++) {
                    row = sheet.createRow(i+1);
                    if(i>1&&rows[i][4]!=null&&rows[i-1][4]!=null&& !StringUtils.equals(rows[i][4],rows[i-1][4])){
                        if(pointer<i) {
                            CellRangeAddress cellAddresses3 = new CellRangeAddress(pointer,i,3,3);
                            sheet.addMergedRegion(cellAddresses3);
                            CellRangeAddress cellAddresses4 = new CellRangeAddress(pointer,i,4,4);
                            sheet.addMergedRegion(cellAddresses4);
                            CellRangeAddress cellAddresses14 = new CellRangeAddress(pointer,i,14,14);
                            sheet.addMergedRegion(cellAddresses14);
                            CellRangeAddress cellAddresses15 = new CellRangeAddress(pointer,i,15,15);
                            sheet.addMergedRegion(cellAddresses15);
                            CellRangeAddress cellAddresses16 = new CellRangeAddress(pointer,i,16,16);
                            sheet.addMergedRegion(cellAddresses16);
                        }
                        pointer = i+1;
                    }else if(i==len-1){
                        if(pointer<i+1) {
                            CellRangeAddress cellAddresses3 = new CellRangeAddress(pointer,i+1,3,3);
                            sheet.addMergedRegion(cellAddresses3);
                            CellRangeAddress cellAddresses4 = new CellRangeAddress(pointer,i+1,4,4);
                            sheet.addMergedRegion(cellAddresses4);
                            CellRangeAddress cellAddresses14 = new CellRangeAddress(pointer,i+1,14,14);
                            sheet.addMergedRegion(cellAddresses14);
                            CellRangeAddress cellAddresses15 = new CellRangeAddress(pointer,i+1,15,15);
                            sheet.addMergedRegion(cellAddresses15);
                            CellRangeAddress cellAddresses16 = new CellRangeAddress(pointer,i+1,16,16);
                            sheet.addMergedRegion(cellAddresses16);
                        }
                    }
                    for (int n = 0; n < count; n++) {
                        cell = row.createCell((short) n);
                        cell.setCellStyle(borderStyle);
                        Object value = rows[i][n];
                        if (value == null) {
                            cell.setCellValue(new XSSFRichTextString(""));
                            continue;
                        }
                        String rtype = value.getClass().getName();
                        if ("java.lang.String".equals(rtype)) {
                            cell.setCellValue(new XSSFRichTextString(value
                                    .toString()));
                        } else if ("java.lang.Integer".equals(rtype)) {
                            cell.setCellValue((Integer) value);
                        } else if ("java.lang.Double".equals(rtype)) {
                            cell.setCellValue((Double) value);
                        } else if ("java.lang.Long".equals(rtype)) {
                            cell.setCellValue((Long) value);
                        }
                    }
                }
            }
        }

        OutputStream output = null;
        try {
            output = response.getOutputStream();
            workbook.write(output);
            output.flush();
        } catch (IOException e) {
//                log.error("buildExcelDocument", e);
        } finally {
            if (output != null) {
                try {
                    output.close();
                    //workbook.close();
                } catch (IOException e) {
//                        log.error("buildExcelDocument", e);
                }
            }

        }
//        return AjaxResult.success(filename);
    }



    /**
     * 新增设备路由信息
     */
    @GetMapping("/addOlt")
    public String addOlt()
    {
        return prefixOlt + "/add";
    }

    /**
     * 新增保存设备路由信息
     */
    @RequiresPermissions("manage:routeInfoOlt:add")
    @Log(title = "设备路由信息", businessType = BusinessType.INSERT)
    @PostMapping("/addOlt")
    @ResponseBody
    public AjaxResult addSave(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        return toAjax(manDeviceRouteInfoService.insertManDeviceRouteInfo(manDeviceRouteInfo));
    }

    /**
     * 修改设备路由信息
     */
    @RequiresPermissions("manage:routeInfoOlt:edit")
    @GetMapping("/editOlt/{id}")
    public String editOlt(@PathVariable("id") Long id, ModelMap mmap)
    {
        ManDeviceRouteInfo manDeviceRouteInfo = manDeviceRouteInfoService.selectManDeviceRouteInfoById(id);
        mmap.put("manDeviceRouteInfo", manDeviceRouteInfo);
        return prefixOlt + "/edit";
    }

    /**
     * 修改保存设备路由信息
     */
    @RequiresPermissions("manage:routeInfoOlt:edit")
    @Log(title = "设备路由信息", businessType = BusinessType.UPDATE)
    @PostMapping("/editOlt")
    @ResponseBody
    public AjaxResult editSaveOlt(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        return toAjax(manDeviceRouteInfoService.updateManDeviceRouteInfo(manDeviceRouteInfo));
    }

    /**
     * 删除设备路由信息
     */
    @RequiresPermissions("manage:routeInfoOlt:remove")
    @Log(title = "设备路由信息", businessType = BusinessType.DELETE)
    @PostMapping( "/removeOlt")
    @ResponseBody
    public AjaxResult removeOlt(String ids)
    {
        return toAjax(manDeviceRouteInfoService.deleteManDeviceRouteInfoByIds(ids));
    }

    @Log(title = "OLT设备路由信息", businessType = BusinessType.IMPORT)
    @RequiresPermissions("manage:routeInfoOlt:import")
    @PostMapping("/importDataOlt")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        InputStream in = file.getInputStream();
        try {
            XSSFWorkbook wb = new XSSFWorkbook(in);

            XSSFSheet sheet = wb.getSheetAt(0);
            List<ManDeviceRouteInfo> infoList = new ArrayList<>();
            //默认第一行为标题行，i = 0
            for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                XSSFRow row = sheet.getRow(i);
                if(row==null) continue;
                ManDeviceRouteInfo info = new ManDeviceRouteInfo();

                String c0 = "";//区县
//                boolean isMerge0 = isMergedRegion(sheet, i, 0);
//                if (isMerge0) {
//                    c0 = getMergedRegionValue(sheet, row.getRowNum(), 0);
//                } else {
                    c0 = getCellValue(row.getCell(0));
//                }
                info.setDeviceCounty(c0);

                String c1 = "";//是否精品
                boolean isMerge1 = isMergedRegion(sheet, i, 1);
                if (isMerge1) {
                    c1 = getMergedRegionValue(sheet, row.getRowNum(), 1);
                } else {
                    c1 = getCellValue(row.getCell(1));
                }
                info.setIsGood(c1);

                String c3 = "";//备注
                boolean isMerge3 = isMergedRegion(sheet, i, 3);
                if (isMerge3) {
                    c3 = getMergedRegionValue(sheet, row.getRowNum(), 3);
                } else {
                    c3 = getCellValue(row.getCell(3));
                }
                info.setRemark(c3);

                String c4 = "";//设备名称
//                boolean isMerge4 = isMergedRegion(sheet, i, 4);
//                if (isMerge4) {
//                    c4 = getMergedRegionValue(sheet, row.getRowNum(), 4);
//                } else {
                c4 = getCellValue(row.getCell(4));
//                }
                info.setDeviceName(c4);

                String c5 = "";//设备IP
//                boolean isMerge5 = isMergedRegion(sheet, i, 5);
//                if (isMerge5) {
//                    c5 = getMergedRegionValue(sheet, row.getRowNum(), 5);
//                } else {
                    c5 = getCellValue(row.getCell(5));
//                }
                info.setDeviceIp(c5);

                String c6 = "";//单播外层vlan
                boolean isMerge6 = isMergedRegion(sheet, i, 6);
                if (isMerge6) {
                    c6 = getMergedRegionValue(sheet, row.getRowNum(), 6);
                } else {
                    c6 = getCellValue(row.getCell(6));
                }
                info.setOutsideVlan(c6);

                String c7 = "";//单播内层vlan
                boolean isMerge7 = isMergedRegion(sheet, i, 7);
                if (isMerge7) {
                    c7 = getMergedRegionValue(sheet, row.getRowNum(), 7);
                } else {
                    c7 = getCellValue(row.getCell(7));
                }
                info.setInnerVlan(c7);

                String c8 = "";//光模块
                boolean isMerge8 = isMergedRegion(sheet, i, 8);
                if (isMerge8) {
                    c8 = getMergedRegionValue(sheet, row.getRowNum(), 8);
                } else {
                    c8 = getCellValue(row.getCell(8));
                }
                info.setOpticalModule(c8);

                String c9 = "";//BAS IP
                boolean isMerge9 = isMergedRegion(sheet, i, 9);
                if (isMerge9) {
                    c9 = getMergedRegionValue(sheet, row.getRowNum(), 9);
                } else {
                    c9 = getCellValue(row.getCell(9));
                }
                info.setBasIp(c9);

                String c10 = "";//BAS 端口
                boolean isMerge10 = isMergedRegion(sheet, i, 10);
                if (isMerge10) {
                    c10 = getMergedRegionValue(sheet, row.getRowNum(), 10);
                } else {
                    c10 = getCellValue(row.getCell(10));
                }
                info.setBasPort(c10);

                String c11 = "";//电路属性
//                boolean isMerge11 = isMergedRegion(sheet, i, 11);
//                if (isMerge11) {
//                    c11 = getMergedRegionValue(sheet, row.getRowNum(), 11);
//                } else {
                    c11 = getCellValue(row.getCell(11));
//                }
                info.setCircuitProperties(c11);

                String c12 = "";//聚合端口
                boolean isMerge12 = isMergedRegion(sheet, i, 12);
                if (isMerge12) {
                    c12 = getMergedRegionValue(sheet, row.getRowNum(), 12);
                } else {
                    c12 = getCellValue(row.getCell(12));
                }
                info.setClosePort(c12);

                String c13 = "";//物理端口
//                boolean isMerge13 = isMergedRegion(sheet, i, 13);
//                if (isMerge13) {
//                    c13 = getMergedRegionValue(sheet, row.getRowNum(), 13);
//                } else {
                    c13 = getCellValue(row.getCell(13));
//                }
                info.setPhysicsPort(c13);

                String c14 = "";//链路带宽
                boolean isMerge14 = isMergedRegion(sheet, i, 14);
                if (isMerge14) {
                    c14 = getMergedRegionValue(sheet, row.getRowNum(), 14);
                } else {
                    c14 = getCellValue(row.getCell(14));
                }
                info.setLinkBandwidth(c14);

                String c15 = "";//对端设备名称
//                boolean isMerge15 = isMergedRegion(sheet, i, 15);
//                if (isMerge15) {
//                    c17 = getMergedRegionValue(sheet, row.getRowNum(), 15);
//                } else {
                c15 = getCellValue(row.getCell(15));
//                }
                info.setOppositeDeviceName(c15);

                String c16 = "";//对端网管名称
//                boolean isMerge16 = isMergedRegion(sheet, i, 16);
//                if (isMerge16) {
//                    c16 = getMergedRegionValue(sheet, row.getRowNum(), 16);
//                } else {
                c16 = getCellValue(row.getCell(16));
//                }
                info.setOppositeNetworkName(c16);

                String c17 = "";//对端设备IP
//                boolean isMerge17 = isMergedRegion(sheet, i, 17);
//                if (isMerge17) {
//                    c17 = getMergedRegionValue(sheet, row.getRowNum(), 17);
//                } else {
                    c17 = getCellValue(row.getCell(17));
//                }
                info.setOppositeDeviceIp(c17);

                String c18 = "";//对端聚合端口
                boolean isMerge18 = isMergedRegion(sheet, i, 18);
                if (isMerge18) {
                    c18 = getMergedRegionValue(sheet, row.getRowNum(), 18);
                } else {
                    c18 = getCellValue(row.getCell(18));
                }
                info.setOpppsiteClosePort(c18);

                String c19 = "";//对端物理端口
//                boolean isMerge19 = isMergedRegion(sheet, i, 19);
//                if (isMerge19) {
//                    c19 = getMergedRegionValue(sheet, row.getRowNum(), 19);
//                } else {
                    c19 = getCellValue(row.getCell(19));
//                }
                info.setOppositePhysicsPort(c19);

                info.setDeviceType("OLT");
                info.setDelFlag("0");
                infoList.add(info);
                //如果到了1000条直接插入  清空列表重新新增
                if(infoList.size()==1000){
                    manDeviceRouteInfoService.insertBatch(infoList);
                    infoList.clear();
                }
            }
            if(infoList.size()>0) manDeviceRouteInfoService.insertBatch(infoList);
        } catch (Exception e) {
            throw new UtilException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(in);
        }
        return AjaxResult.success("导入成功");
    }


    /**
     * 获取单元格的值
     * @param cell
     * @return
     */
    public static String getCellValue(Cell cell) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        String cellValue = "";
        if (cell == null) {
            return cellValue;
        }

//        CellType cellType = cell.getCellTypeEnum();
//        // 把数字当成String来读，避免出现1读成1.0的情况
//        if (cellType == CellType.NUMERIC) {
//            cellType = CellType.STRING;
//        }
        cell.setCellType(CellType.STRING);

        // 判断数据的类型
        switch (cell.getCellTypeEnum()) {
            case NUMERIC: // 数字、日期
                if (DateUtil.isCellDateFormatted(cell)) {
                    cellValue = fmt.format(cell.getDateCellValue()); // 日期型
                } else {
                    cellValue = String.valueOf(cell.getStringCellValue());
//                cellValue = String.valueOf(cell.getNumericCellValue()); // 数字
                    if (cellValue.contains("E")) {
                        cellValue = String.valueOf(new Double(cell.getNumericCellValue()).longValue()); // 数字
                    }
                }
                break;
            case STRING: // 字符串
                cellValue = String.valueOf(cell.getStringCellValue());
                break;
            case BOOLEAN: // Boolean
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case FORMULA: // 公式
                cellValue = String.valueOf(cell.getCellFormula());
                break;
            case BLANK: // 空值
                cellValue = cell.getStringCellValue();
                break;
            case ERROR: // 故障
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue.trim();
    }

//    public static String getCellValue(Cell cell) {
//        if (cell == null) return null;
//        String retValue = null;
//        switch (cell.getCellType()) {
//            case NUMERIC: //数字
////                retValue = String.valueOf(cell.getNumericCellValue()).trim();
//                retValue = cell.getStringCellValue().trim();
//                break;
//            case STRING: //字符串
//                retValue = cell.getStringCellValue().trim();
//                break;
//            case BOOLEAN: //Boolean
//                retValue = String.valueOf(cell.getBooleanCellValue()).trim();
//                break;
//            case FORMULA: //公式
////                retValue = String.valueOf(cell.getNumericCellValue()).trim();
//                retValue = cell.getStringCellValue().trim();
//                break;
//            case BLANK://空值
//                retValue = "";
//                break;
//            case ERROR://故障
//                break;
//            default:
//                break;
//        }
//        return retValue;
//    }

    /**
     * 判断指定的单元格是否是合并单元格
     *
     * @param sheet
     * @param row    行下标
     * @param column 列下标
     * @return
     */
    private static boolean isMergedRegion(Sheet sheet, int row, int column) {
        //获取该sheet所有合并的单元格
        int sheetMergeCount = sheet.getNumMergedRegions();
        //循环判断 该单元格属于哪个合并单元格， 如果能找到对应的，就表示该单元格是合并单元格
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if (row >= firstRow && row <= lastRow) {
                if (column >= firstColumn && column <= lastColumn) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * 获取合并单元格的值
     *
     * @param sheet  sheet索引 从0开始
     * @param row    行索引 从0开始
     * @param column 列索引  从0开始
     * @return
     */
    public static String getMergedRegionValue(Sheet sheet, int row, int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress ca = sheet.getMergedRegion(i);
            int firstColumn = ca.getFirstColumn();
            int lastColumn = ca.getLastColumn();
            int firstRow = ca.getFirstRow();
            int lastRow = ca.getLastRow();
            if (row >= firstRow && row <= lastRow) {
                if (column >= firstColumn && column <= lastColumn) {
                    Row fRow = sheet.getRow(firstRow);
                    Cell fCell = fRow.getCell(firstColumn);
                    return getCellValue(fCell);
                }
            }
        }
        return null;
    }
    /*****************************************   OLT设备  END   ***************************************/

    /*****************************************   SW设备  START   ***************************************/

    private String prefixSw= "manage/routeInfoSw";

    @RequiresPermissions("manage:routeInfoSw:view")
    @GetMapping("/routeInfoSw")
    public String routeInfoSw()
    {
        return prefixSw + "/routeInfoSw";
    }

    /**
     * 查询OLT设备路由信息列表
     */
    @RequiresPermissions("manage:routeInfoSw:list")
    @PostMapping("/listSw")
    @ResponseBody
    public TableDataInfo listSw(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        startPage();
        List<ManDeviceRouteInfo> list = manDeviceRouteInfoService.selectManDeviceRouteInfoListSw(manDeviceRouteInfo);
        return getDataTable(list);
    }

    /**
     * 导出设备路由信息列表
     */
    @RequiresPermissions("manage:routeInfoSw:export")
    @Log(title = "SW设备路由信息", businessType = BusinessType.EXPORT)
    @PostMapping("/exportSw")
    @ResponseBody
    public AjaxResult exportSw(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        List<ManDeviceRouteInfo> list = manDeviceRouteInfoService.selectManDeviceRouteInfoListOlt(manDeviceRouteInfo);
        ExcelUtil<ManDeviceRouteInfo> util = new ExcelUtil<ManDeviceRouteInfo>(ManDeviceRouteInfo.class);
        return util.exportExcel(list, "设备路由信息数据");
    }

    /**
     * 新增设备路由信息
     */
    @GetMapping("/addSw")
    public String addSw()
    {
        return prefixSw + "/add";
    }

    /**
     * 新增保存设备路由信息
     */
    @RequiresPermissions("manage:routeInfoSw:add")
    @Log(title = "SW设备路由信息", businessType = BusinessType.INSERT)
    @PostMapping("/addSw")
    @ResponseBody
    public AjaxResult addSaveSw(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        return toAjax(manDeviceRouteInfoService.insertManDeviceRouteInfo(manDeviceRouteInfo));
    }

    /**
     * 修改设备路由信息
     */
    @RequiresPermissions("manage:routeInfoSw:edit")
    @GetMapping("/editSw/{id}")
    public String editSw(@PathVariable("id") Long id, ModelMap mmap)
    {
        ManDeviceRouteInfo manDeviceRouteInfo = manDeviceRouteInfoService.selectManDeviceRouteInfoById(id);
        mmap.put("manDeviceRouteInfo", manDeviceRouteInfo);
        return prefixSw + "/edit";
    }

    /**
     * 修改保存设备路由信息
     */
    @RequiresPermissions("manage:routeInfoSw:edit")
    @Log(title = "SW设备路由信息", businessType = BusinessType.UPDATE)
    @PostMapping("/editSw")
    @ResponseBody
    public AjaxResult editSaveSw(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        return toAjax(manDeviceRouteInfoService.updateManDeviceRouteInfo(manDeviceRouteInfo));
    }

    /**
     * 删除设备路由信息
     */
    @RequiresPermissions("manage:routeInfoSw:remove")
    @Log(title = "设备路由信息", businessType = BusinessType.DELETE)
    @PostMapping( "/removeSw")
    @ResponseBody
    public AjaxResult removeSw(String ids)
    {
        return toAjax(manDeviceRouteInfoService.deleteManDeviceRouteInfoByIds(ids));
    }

    @Log(title = "SW设备路由信息", businessType = BusinessType.IMPORT)
    @RequiresPermissions("manage:routeInfoSw:import")
    @PostMapping("/importDataSw")
    @ResponseBody
    public AjaxResult importDataSW(MultipartFile file, boolean updateSupport) throws Exception {
        InputStream in = file.getInputStream();
        try {
            XSSFWorkbook wb = new XSSFWorkbook(in);

            XSSFSheet sheet = wb.getSheetAt(0);
            List<ManDeviceRouteInfo> infoList = new ArrayList<>();
            //默认第一行为标题行，i = 0
            for (int i = 1; i < 530; i++) {
                XSSFRow row = sheet.getRow(i);
                if(row==null) continue;
                ManDeviceRouteInfo info = new ManDeviceRouteInfo();

                String c0 = "";//区县
                boolean isMerge0 = isMergedRegion(sheet, i, 0);
                if (isMerge0) {
                    c0 = getMergedRegionValue(sheet, row.getRowNum(), 0);
                } else {
                    c0 = getCellValue(row.getCell(0));
                }
                info.setDeviceCounty(c0);

                String c1 = "";//设备位置
                boolean isMerge1 = isMergedRegion(sheet, i, 1);
                if (isMerge1) {
                    c1 = getMergedRegionValue(sheet, row.getRowNum(), 1);
                } else {
                    c1 = getCellValue(row.getCell(1));
                }
                info.setDevicePath(c1);


                String c2 = "";//网管名称
//                boolean isMerge2 = isMergedRegion(sheet, i, 2);
//                if (isMerge2) {
//                    c2 = getMergedRegionValue(sheet, row.getRowNum(), 2);
//                } else {
                    c2 = getCellValue(row.getCell(2));
//                }
                info.setNetworkName(c2);

                String c3 = "";//设备名称
//                boolean isMerge3 = isMergedRegion(sheet, i, 3);
//                if (isMerge3) {
//                    c3 = getMergedRegionValue(sheet, row.getRowNum(), 3);
//                } else {
                    c3 = getCellValue(row.getCell(3));
//                }
                info.setDeviceName(c3);

                String c4 = "";//设备IP
//                boolean isMerge4 = isMergedRegion(sheet, i, 4);
//                if (isMerge4) {
//                    c4 = getMergedRegionValue(sheet, row.getRowNum(), 4);
//                } else {
                    c4 = getCellValue(row.getCell(4));
//                }
                info.setDeviceIp(c4);

                String c5 = "";//光模块
                boolean isMerge5 = isMergedRegion(sheet, i, 5);
                if (isMerge5) {
                    c5 = getMergedRegionValue(sheet, row.getRowNum(), 5);
                } else {
                    c5 = getCellValue(row.getCell(5));
                }
                info.setOpticalModule(c5);

                String c6 = "";//电路属性
                boolean isMerge6 = isMergedRegion(sheet, i, 6);
                if (isMerge6) {
                    c6 = getMergedRegionValue(sheet, row.getRowNum(), 6);
                } else {
                    c6 = getCellValue(row.getCell(6));
                }
                info.setCircuitProperties(c6);

                String c7 = "";//链路带宽
                boolean isMerge7 = isMergedRegion(sheet, i, 7);
                if (isMerge7) {
                    c7 = getMergedRegionValue(sheet, row.getRowNum(), 7);
                } else {
                    c7 = getCellValue(row.getCell(7));
                }
                info.setLinkBandwidth(c7);

                String c8 = "";//聚合端口
                boolean isMerge8 = isMergedRegion(sheet, i, 8);
                if (isMerge8) {
                    c8 = getMergedRegionValue(sheet, row.getRowNum(), 8);
                } else {
                    c8 = getCellValue(row.getCell(8));
                }
                info.setClosePort(c8);

                String c9 = "";//物理端口
                boolean isMerge9 = isMergedRegion(sheet, i, 9);
                if (isMerge9) {
                    c9 = getMergedRegionValue(sheet, row.getRowNum(), 9);
                } else {
                    c9 = getCellValue(row.getCell(9));
                }
                info.setPhysicsPort(c9);

                String c10 = "";//对端设备名称
//                boolean isMerge10 = isMergedRegion(sheet, i, 10);
//                if (isMerge10) {
//                    c10 = getMergedRegionValue(sheet, row.getRowNum(), 10);
//                } else {
                    c10 = getCellValue(row.getCell(10));
//                }
                info.setOppositeDeviceName(c10);

                String c11 = "";//对端设备IP
//                boolean isMerge11 = isMergedRegion(sheet, i, 11);
//                if (isMerge11) {
//                    c11 = getMergedRegionValue(sheet, row.getRowNum(), 11);
//                } else {
                    c11 = getCellValue(row.getCell(11));
//                }
                info.setOppositeDeviceIp(c11);

                String c12 = "";//对端聚合端口
                boolean isMerge12 = isMergedRegion(sheet, i, 12);
                if (isMerge12) {
                    c12 = getMergedRegionValue(sheet, row.getRowNum(), 12);
                } else {
                    c12 = getCellValue(row.getCell(12));
                }
                info.setOpppsiteClosePort(c12);

                String c13 = "";//对端物理端口
                boolean isMerge13 = isMergedRegion(sheet, i, 13);
                if (isMerge13) {
                    c13 = getMergedRegionValue(sheet, row.getRowNum(), 13);
                } else {
                    c13 = getCellValue(row.getCell(13));
                }
                info.setOppositePhysicsPort(c13);

                info.setDeviceType("SW");
                info.setDelFlag("0");
                infoList.add(info);
                //如果到了1000条直接插入  清空列表重新新增
                if(infoList.size()==1000){
                    manDeviceRouteInfoService.insertBatch(infoList);
                    infoList.clear();
                }
            }
            if(infoList.size()>0) manDeviceRouteInfoService.insertBatch(infoList);
        } catch (Exception e) {
            throw new UtilException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(in);
        }
        return AjaxResult.success("导入成功");
    }


    /*****************************************   SW设备  END   ***************************************/

    /*****************************************   CR设备  START   ***************************************/

    private String prefixCr= "manage/routeInfoCr";

    @RequiresPermissions("manage:routeInfoCr:view")
    @GetMapping("/routeInfoCr")
    public String routeInfoCr() {
        return prefixCr + "/routeInfoCr";
    }

    /**
     * 查询Cr设备路由信息列表
     */
    @RequiresPermissions("manage:routeInfoCr:list")
    @PostMapping("/listCr")
    @ResponseBody
    public TableDataInfo listCr(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        startPage();
        List<ManDeviceRouteInfo> list = manDeviceRouteInfoService.selectManDeviceRouteInfoListCr(manDeviceRouteInfo);
        return getDataTable(list);
    }

    /**
     * 导出设备路由信息列表
     */
    @RequiresPermissions("manage:routeInfoCr:export")
    @Log(title = "SW设备路由信息", businessType = BusinessType.EXPORT)
    @PostMapping("/exportCr")
    @ResponseBody
    public AjaxResult exportCr(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        List<ManDeviceRouteInfo> list = manDeviceRouteInfoService.selectManDeviceRouteInfoListCr(manDeviceRouteInfo);
        ExcelUtil<ManDeviceRouteInfo> util = new ExcelUtil<ManDeviceRouteInfo>(ManDeviceRouteInfo.class);
        return util.exportExcel(list, "设备路由信息数据");
    }

    /**
     * 新增设备路由信息
     */
    @GetMapping("/addCr")
    public String addCr()
    {
        return prefixCr + "/add";
    }

    /**
     * 新增保存设备路由信息
     */
    @RequiresPermissions("manage:routeInfoCr:add")
    @Log(title = "CR设备路由信息", businessType = BusinessType.INSERT)
    @PostMapping("/addCr")
    @ResponseBody
    public AjaxResult addSaveCr(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        return toAjax(manDeviceRouteInfoService.insertManDeviceRouteInfo(manDeviceRouteInfo));
    }

    /**
     * 修改设备路由信息
     */
    @RequiresPermissions("manage:routeInfoCr:edit")
    @GetMapping("/editCr/{id}")
    public String editCr(@PathVariable("id") Long id, ModelMap mmap)
    {
        ManDeviceRouteInfo manDeviceRouteInfo = manDeviceRouteInfoService.selectManDeviceRouteInfoById(id);
        mmap.put("manDeviceRouteInfo", manDeviceRouteInfo);
        return prefixSw + "/edit";
    }

    /**
     * 修改保存设备路由信息
     */
    @RequiresPermissions("manage:routeInfoCr:edit")
    @Log(title = "CR设备路由信息", businessType = BusinessType.UPDATE)
    @PostMapping("/editCr")
    @ResponseBody
    public AjaxResult editSaveCr(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        return toAjax(manDeviceRouteInfoService.updateManDeviceRouteInfo(manDeviceRouteInfo));
    }

    /**
     * 删除设备路由信息
     */
    @RequiresPermissions("manage:routeInfoSw:remove")
    @Log(title = "设备路由信息", businessType = BusinessType.DELETE)
    @PostMapping( "/removeCr")
    @ResponseBody
    public AjaxResult removeCr(String ids)
    {
        return toAjax(manDeviceRouteInfoService.deleteManDeviceRouteInfoByIds(ids));
    }

    @Log(title = "SW设备路由信息", businessType = BusinessType.IMPORT)
    @RequiresPermissions("manage:routeInfoSw:import")
    @PostMapping("/importDataCr")
    @ResponseBody
    public AjaxResult importDataCr(MultipartFile file, boolean updateSupport) throws Exception {
        InputStream in = file.getInputStream();
        try {
            XSSFWorkbook wb = new XSSFWorkbook(in);

            XSSFSheet sheet = wb.getSheetAt(0);
            List<ManDeviceRouteInfo> infoList = new ArrayList<>();
            //默认第一行为标题行，i = 0
            for (int i = 1; i < 530; i++) {
                XSSFRow row = sheet.getRow(i);
                if(row==null) continue;
                ManDeviceRouteInfo info = new ManDeviceRouteInfo();

                String c0 = "";//区县
                boolean isMerge0 = isMergedRegion(sheet, i, 0);
                if (isMerge0) {
                    c0 = getMergedRegionValue(sheet, row.getRowNum(), 0);
                } else {
                    c0 = getCellValue(row.getCell(0));
                }
                info.setDeviceCounty(c0);

                String c1 = "";//设备位置
                boolean isMerge1 = isMergedRegion(sheet, i, 1);
                if (isMerge1) {
                    c1 = getMergedRegionValue(sheet, row.getRowNum(), 1);
                } else {
                    c1 = getCellValue(row.getCell(1));
                }
                info.setDevicePath(c1);


                String c2 = "";//网管名称
//                boolean isMerge2 = isMergedRegion(sheet, i, 2);
//                if (isMerge2) {
//                    c2 = getMergedRegionValue(sheet, row.getRowNum(), 2);
//                } else {
                c2 = getCellValue(row.getCell(2));
//                }
                info.setNetworkName(c2);

                String c3 = "";//设备名称
//                boolean isMerge3 = isMergedRegion(sheet, i, 3);
//                if (isMerge3) {
//                    c3 = getMergedRegionValue(sheet, row.getRowNum(), 3);
//                } else {
                c3 = getCellValue(row.getCell(3));
//                }
                info.setDeviceName(c3);

                String c4 = "";//设备IP
//                boolean isMerge4 = isMergedRegion(sheet, i, 4);
//                if (isMerge4) {
//                    c4 = getMergedRegionValue(sheet, row.getRowNum(), 4);
//                } else {
                c4 = getCellValue(row.getCell(4));
//                }
                info.setDeviceIp(c4);

                String c5 = "";//光模块
                boolean isMerge5 = isMergedRegion(sheet, i, 5);
                if (isMerge5) {
                    c5 = getMergedRegionValue(sheet, row.getRowNum(), 5);
                } else {
                    c5 = getCellValue(row.getCell(5));
                }
                info.setOpticalModule(c5);

                String c6 = "";//电路属性
                boolean isMerge6 = isMergedRegion(sheet, i, 6);
                if (isMerge6) {
                    c6 = getMergedRegionValue(sheet, row.getRowNum(), 6);
                } else {
                    c6 = getCellValue(row.getCell(6));
                }
                info.setCircuitProperties(c6);

                String c7 = "";//链路带宽
                boolean isMerge7 = isMergedRegion(sheet, i, 7);
                if (isMerge7) {
                    c7 = getMergedRegionValue(sheet, row.getRowNum(), 7);
                } else {
                    c7 = getCellValue(row.getCell(7));
                }
                info.setLinkBandwidth(c7);

                String c8 = "";//聚合端口
                boolean isMerge8 = isMergedRegion(sheet, i, 8);
                if (isMerge8) {
                    c8 = getMergedRegionValue(sheet, row.getRowNum(), 8);
                } else {
                    c8 = getCellValue(row.getCell(8));
                }
                info.setClosePort(c8);

                String c9 = "";//物理端口
                boolean isMerge9 = isMergedRegion(sheet, i, 9);
                if (isMerge9) {
                    c9 = getMergedRegionValue(sheet, row.getRowNum(), 9);
                } else {
                    c9 = getCellValue(row.getCell(9));
                }
                info.setPhysicsPort(c9);

                String c10 = "";//对端设备名称
//                boolean isMerge10 = isMergedRegion(sheet, i, 10);
//                if (isMerge10) {
//                    c10 = getMergedRegionValue(sheet, row.getRowNum(), 10);
//                } else {
                c10 = getCellValue(row.getCell(10));
//                }
                info.setOppositeDeviceName(c10);

                String c11 = "";//对端设备IP
//                boolean isMerge11 = isMergedRegion(sheet, i, 11);
//                if (isMerge11) {
//                    c11 = getMergedRegionValue(sheet, row.getRowNum(), 11);
//                } else {
                c11 = getCellValue(row.getCell(11));
//                }
                info.setOppositeDeviceIp(c11);

                String c12 = "";//对端聚合端口
                boolean isMerge12 = isMergedRegion(sheet, i, 12);
                if (isMerge12) {
                    c12 = getMergedRegionValue(sheet, row.getRowNum(), 12);
                } else {
                    c12 = getCellValue(row.getCell(12));
                }
                info.setOpppsiteClosePort(c12);

                String c13 = "";//对端物理端口
                boolean isMerge13 = isMergedRegion(sheet, i, 13);
                if (isMerge13) {
                    c13 = getMergedRegionValue(sheet, row.getRowNum(), 13);
                } else {
                    c13 = getCellValue(row.getCell(13));
                }
                info.setOppositePhysicsPort(c13);

                info.setDeviceType("SW");
                info.setDelFlag("0");
                infoList.add(info);
                //如果到了1000条直接插入  清空列表重新新增
                if(infoList.size()==1000){
                    manDeviceRouteInfoService.insertBatch(infoList);
                    infoList.clear();
                }
            }
            if(infoList.size()>0) manDeviceRouteInfoService.insertBatch(infoList);
        } catch (Exception e) {
            throw new UtilException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(in);
        }
        return AjaxResult.success("导入成功");
    }


    /*****************************************   CR设备  END   ***************************************/
}
