package com.ruoyi.web.controller.manage;

import java.util.List;

import com.ruoyi.manage.domain.ManVlanPon;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.manage.service.IManVlanPonService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * vlan宽带PON管理Controller
 * 
 * @author LR
 * @date 2024-02-20
 */
@Controller
@RequestMapping("/manage/vlanPon")
public class ManVlanPonController extends BaseController
{
    private String prefix = "manage/vlanPon";

    @Autowired
    private IManVlanPonService manVlanPonService;

    @RequiresPermissions("manage:vlanPon:view")
    @GetMapping()
    public String vlanPon()
    {
        return prefix + "/vlanPon";
    }

    /**
     * 查询vlan宽带PON管理列表
     */
    @RequiresPermissions("manage:vlanPon:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ManVlanPon manVlanPon)
    {
        startPage();
        List<ManVlanPon> list = manVlanPonService.selectManVlanPonList(manVlanPon);
        return getDataTable(list);
    }

    /**
     * 导出vlan宽带PON管理列表
     */
    @RequiresPermissions("manage:vlanPon:export")
    @Log(title = "vlan宽带PON管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ManVlanPon manVlanPon)
    {
        List<ManVlanPon> list = manVlanPonService.selectManVlanPonList(manVlanPon);
        ExcelUtil<ManVlanPon> util = new ExcelUtil<ManVlanPon>(ManVlanPon.class);
        return util.exportExcel(list, "vlan宽带PON管理数据");
    }

//
//    /**
//     * 新增vlan宽带PON管理
//     */
//    @GetMapping("/add")
//    public String add()
//    {
//        return prefix + "/add";
//    }

    /**
     * 新增vlan宽带PON管理
     */
    @GetMapping("/add/{deviceId}")
    public String add(@PathVariable("deviceId") Long deviceId, ModelMap mmap)
    {
        mmap.put("deviceId", deviceId);
        return prefix + "/add";
    }

    /**
     * 新增保存vlan宽带PON管理
     */
    @RequiresPermissions("manage:vlanPon:add")
    @Log(title = "vlan宽带PON管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ManVlanPon manVlanPon)
    {
        return toAjax(manVlanPonService.insertManVlanPon(manVlanPon));
    }

    /**
     * 修改vlan宽带PON管理
     */
    @RequiresPermissions("manage:vlanPon:edit")
    @GetMapping("/edit/{ponId}")
    public String edit(@PathVariable("ponId") Long ponId, ModelMap mmap)
    {
        ManVlanPon manVlanPon = manVlanPonService.selectManVlanPonByPonId(ponId);
        mmap.put("manVlanPon", manVlanPon);
        return prefix + "/edit";
    }

    /**
     * 修改保存vlan宽带PON管理
     */
    @RequiresPermissions("manage:vlanPon:edit")
    @Log(title = "vlan宽带PON管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ManVlanPon manVlanPon)
    {
        return toAjax(manVlanPonService.updateManVlanPon(manVlanPon));
    }

    /**
     * 删除vlan宽带PON管理
     */
    @RequiresPermissions("manage:vlanPon:remove")
    @Log(title = "vlan宽带PON管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(manVlanPonService.deleteManVlanPonByPonIds(ids));
    }
}
