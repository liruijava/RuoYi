package com.ruoyi.web.controller.manage;

import java.util.List;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.manage.domain.ManVlanArea;
import com.ruoyi.manage.service.IManVlanAreaService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * vlan宽带规划区域Controller
 * 
 * @author LR
 * @date 2024-02-19
 */
@Controller
@RequestMapping("/manage/vlanArea")
public class ManVlanAreaController extends BaseController
{
    private String prefix = "manage/vlanArea";

    @Autowired
    private IManVlanAreaService manVlanAreaService;

    @RequiresPermissions("manage:vlanArea:view")
    @GetMapping()
    public String area()
    {
        return prefix + "/vlanArea";
    }

    /**
     * 查询vlan宽带规划区域列表
     */
    @RequiresPermissions("manage:vlanArea:list")
    @PostMapping("/list")
    @ResponseBody
    public List<ManVlanArea> list(ManVlanArea manVlanArea)
    {
        List<ManVlanArea> list = manVlanAreaService.selectManVlanAreaList(manVlanArea);
        return list;
    }

    /**
     * 查询设备详细
     */
    @RequiresPermissions("manage:vlanArea:list")
    @GetMapping("/view/{areaId}")
    public String view(@PathVariable("areaId") Long areaId, ModelMap mmap)
    {
        mmap.put("vlanArea", manVlanAreaService.selectManVlanAreaByAreaId(areaId));
        return prefix + "/view";
    }

    /**
     * 导出vlan宽带规划区域列表
     */
    @RequiresPermissions("manage:vlanArea:export")
    @Log(title = "vlan宽带规划区域", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ManVlanArea manVlanArea)
    {
        List<ManVlanArea> list = manVlanAreaService.selectManVlanAreaList(manVlanArea);
        ExcelUtil<ManVlanArea> util = new ExcelUtil<ManVlanArea>(ManVlanArea.class);
        return util.exportExcel(list, "vlan宽带规划区域数据");
    }

//    /**
//     * 新增vlan宽带规划区域
//     */
//    @GetMapping("/add")
//    public String add()
//    {
//        return prefix + "/add";
//    }

    /**
     * 新增vlan宽带规划区域
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, ModelMap mmap)
    {
        if (!getSysUser().isAdmin())
        {
            parentId = getSysUser().getDeptId();
        }
        mmap.put("vlanArea", manVlanAreaService.selectManVlanAreaByAreaId(parentId));
        return prefix + "/add";
    }

    /**
     * 新增保存vlan宽带规划区域
     */
    @RequiresPermissions("manage:vlanArea:add")
    @Log(title = "vlan宽带规划区域", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ManVlanArea manVlanArea)
    {
        return toAjax(manVlanAreaService.insertManVlanArea(manVlanArea));
    }

    /**
     * 修改vlan宽带规划区域
     */
    @RequiresPermissions("manage:vlanArea:edit")
    @GetMapping("/edit/{areaId}")
    public String edit(@PathVariable("areaId") Long areaId, ModelMap mmap)
    {
        ManVlanArea manVlanArea = manVlanAreaService.selectManVlanAreaByAreaId(areaId);
        mmap.put("vlanArea", manVlanArea);
        return prefix + "/edit";
    }

    /**
     * 修改保存vlan宽带规划区域
     */
    @RequiresPermissions("manage:vlanArea:edit")
    @Log(title = "vlan宽带规划区域", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ManVlanArea manVlanArea)
    {
        return toAjax(manVlanAreaService.updateManVlanArea(manVlanArea));
    }


//    /**
//     * 删除vlan宽带规划区域
//     */
//    @RequiresPermissions("manage:vlanArea:remove")
//    @Log(title = "vlan宽带规划区域", businessType = BusinessType.DELETE)
//    @PostMapping( "/remove")
//    @ResponseBody
//    public AjaxResult remove(String ids)
//    {
//        return toAjax(manVlanAreaService.deleteManVlanAreaByAreaIds(ids));
//    }

    /**
     * 删除vlan宽带规划区域
     */
    @Log(title = "vlan宽带规划区域", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:dept:remove")
    @GetMapping("/remove/{areaId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("areaId") Long areaId)
    {
        ManVlanArea area = new ManVlanArea();
        area.setParentId(areaId);
        List<ManVlanArea> list = manVlanAreaService.selectManVlanAreaList(area);
        if (list.size() > 0)
        {
            return AjaxResult.warn("存在下级区域,不允许删除");
        }
        return toAjax(manVlanAreaService.deleteManVlanAreaByAreaId(areaId));
    }


    /**
     * 选择区域树
     *
     * @param areaId 区域ID
     * @param excludeId 排除ID
     */
    @GetMapping(value = { "/selectAreaTree/{areaId}", "/selectAreaTree/{areaId}/{excludeId}" })
    public String selectDeptTree(@PathVariable("areaId") Long areaId,
                                 @PathVariable(value = "excludeId", required = false) Long excludeId, ModelMap mmap)
    {
        mmap.put("vlanArea", manVlanAreaService.selectManVlanAreaByAreaId(areaId));
        mmap.put("excludeId", excludeId);
        return prefix + "/tree";
    }

    /**
     * 加载区域列表树（排除下级）
     */
    @GetMapping("/treeData/{excludeId}")
    @ResponseBody
    public List<Ztree> treeDataExcludeChild(@PathVariable(value = "excludeId", required = false) Long excludeId)
    {
        ManVlanArea area = new ManVlanArea();
        area.setExcludeId(excludeId);
        List<Ztree> ztrees = manVlanAreaService.selectAreaTreeExcludeChild(area);
        return ztrees;
    }
}
