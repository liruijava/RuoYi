package com.ruoyi.web.controller.manage;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.exception.UtilException;
import com.ruoyi.manage.domain.ExcelMergeVo;
import com.ruoyi.manage.domain.ManVlanArea;
import com.ruoyi.manage.domain.ManVlanDevice;
import com.ruoyi.manage.domain.ManVlanPon;
import com.ruoyi.manage.service.IManVlanAreaService;
import com.ruoyi.manage.service.IManVlanDeviceService;
import com.ruoyi.manage.service.IManVlanPonService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * vlan宽带设备Controller
 * 
 * @author LR
 * @date 2024-02-20
 */
@Controller
@RequestMapping("/manage/vlanDevice")
public class ManVlanDeviceController extends BaseController
{
    private String prefix = "manage/vlanDevice";

    @Autowired
    private IManVlanDeviceService manVlanDeviceService;

    @Autowired
    private IManVlanAreaService manVlanAreaService;

    @Autowired
    private IManVlanPonService manVlanPonService;

    @RequiresPermissions("manage:vlanDevice:view")
    @GetMapping()
    public String device()
    {
        return prefix + "/vlanDevice";
    }

    /**
     * 查询vlan宽带设备列表
     */
    @RequiresPermissions("manage:vlanDevice:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ManVlanDevice manVlanDevice)
    {
        startPage();
        List<ManVlanDevice> list = manVlanDeviceService.selectManVlanDeviceList(manVlanDevice);
        return getDataTable(list);
    }

    /**
     * 新增vlan宽带设备
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存vlan宽带设备
     */
    @RequiresPermissions("manage:vlanDevice:add")
    @Log(title = "vlan宽带设备", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ManVlanDevice manVlanDevice)
    {
        return toAjax(manVlanDeviceService.insertManVlanDevice(manVlanDevice));
    }

    /**
     * 修改vlan宽带设备
     */
    @RequiresPermissions("manage:vlanDevice:edit")
    @GetMapping("/edit/{deviceId}")
    public String edit(@PathVariable("deviceId") Long deviceId, ModelMap mmap)
    {
        ManVlanDevice manVlanDevice = manVlanDeviceService.selectManVlanDeviceByDeviceId(deviceId);
        mmap.put("vlanDevice", manVlanDevice);
        return prefix + "/edit";
    }

    /**
     * 修改保存vlan宽带设备
     */
    @RequiresPermissions("manage:vlanDevice:edit")
    @Log(title = "vlan宽带设备", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ManVlanDevice manVlanDevice)
    {
        return toAjax(manVlanDeviceService.updateManVlanDevice(manVlanDevice));
    }

    /**
     * 删除vlan宽带设备
     */
    @RequiresPermissions("manage:vlanDevice:remove")
    @Log(title = "vlan宽带设备", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(manVlanDeviceService.deleteManVlanDeviceByDeviceIds(ids));
    }


    /**
     * 加载区域列表树
     */
    @RequiresPermissions("manage:vlanDevice:list")
    @GetMapping("/areaTreeData")
    @ResponseBody
    public List<Ztree> areaTreeData()
    {
        List<Ztree> ztrees = manVlanAreaService.selectVlanAreaTree(new ManVlanArea());
        return ztrees;
    }

    /**
     * 选择区域树
     *
     * @param areaId 区域ID
     */
    @RequiresPermissions("manage:vlanDevice:list")
    @GetMapping("/selectAreaTree/{areaId}")
    public String selectDeptTree(@PathVariable("areaId") Long areaId, ModelMap mmap)
    {
        mmap.put("vlanArea", manVlanAreaService.selectManVlanAreaByAreaId(areaId));
        return prefix + "/areaTree";
    }

    /**
     * 进入PON管理页
     */
    @GetMapping("/ponManage/{deviceId}")
    public String authRole(@PathVariable("deviceId") Long deviceId, ModelMap mmap)
    {
        ManVlanDevice manVlanDevice = manVlanDeviceService.selectManVlanDeviceByDeviceId(deviceId);
        // 获取用户所属的角色列表
        mmap.put("vlanDevice", manVlanDevice);
//        mmap.put("roles", SysUser.isAdmin(userId) ? roles : roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
        return prefix + "/ponManage";
    }

    /**
     * 查询设备详细
     */
    @RequiresPermissions("manage:vlanDevice:list")
    @GetMapping("/view/{deviceId}")
    public String view(@PathVariable("deviceId") Long deviceId, ModelMap mmap)
    {
        mmap.put("vlanDevice", manVlanDeviceService.selectManVlanDeviceByDeviceId(deviceId));
        return prefix + "/view";
    }


    /**
     * 导出vlan宽带设备列表
     */
    @RequiresPermissions("manage:vlanDevice:export")
    @Log(title = "vlan宽带设备", businessType = BusinessType.EXPORT)
    @RequestMapping("/export")
    @ResponseBody
    public void export(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<ManVlanArea> list = manVlanAreaService.selectAreaListWithDevice(new ManVlanArea());
        if (list.size()==0) {
            throw new Exception("查询数据为空，无法导出");
        }
        String[] headers = new String[]{
                "设备名称", "板卡编码", "PON口编码", "资源地址","业务类型","外层vlan","内层vlan","划分日期"
        };
        Map<String,Object> map = builderAttentionDataMap(list,headers);
        exportExcel(map,headers,request,response);
    }

    private Map<String, Object> builderAttentionDataMap(List<ManVlanArea> list, String[] headers) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        for (ManVlanArea area :list){
            if(area.getPonList()==null||area.getPonList().size()==0){
                dataMap.put(area.getAreaName(), null);
                continue;
            }
            String[][] rows = new String[area.getPonList().size()][headers.length];
            for (int i = 0; i < area.getPonList().size(); i++) {
                ManVlanPon pon =  area.getPonList().get(i);
                rows[i][0] = pon.getDeviceName() == null ? "" : pon.getDeviceName() ;
                rows[i][1] = pon.getSlot() == null ? "" : pon.getSlot() ;
                rows[i][2] = pon.getPonName() == null ? "" : pon.getPonName() ;
                rows[i][3] = pon.getPonPath() == null ? "" : pon.getPonPath() ;
                rows[i][4] = pon.getBusinessType() == null ? "" : pon.getBusinessType() ;
                rows[i][5] = pon.getOutsideVlan() == null ? "" : pon.getOutsideVlan() ;
                rows[i][6] = pon.getInnerVlan() == null ? "" : pon.getInnerVlan() ;
                rows[i][7] = pon.getClassifyDate() == null ? "" : pon.getClassifyDate() ;
            }
            dataMap.put(area.getAreaName(), rows);
        }
        return dataMap;
    }

    /**
     * 对list数据源将其里面的数据导入到excel表单
     *
     * @return 结果
     */
    public void exportExcel(Map<String,Object> dataMap, String[] headers, HttpServletRequest request, HttpServletResponse response) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String filename = df.format(new Date())+"VLAN宽带规划.xlsx";

        String agent = request.getHeader("USER-AGENT");
        if (agent != null && agent.toLowerCase().indexOf("firefox") > 0) {
            filename = "=?UTF-8?B?" + (new String(Base64Utils.encodeToString(filename.getBytes("UTF-8")))) + "?=";
        } else {
            filename = URLEncoder.encode(filename, "UTF-8");
        }


        response.setContentType("application/x-msdownload");
        response.setHeader("Content-Disposition", "attachment;filename=" + filename);
//		response.setHeader("Content-Disposition", "attachment; fileName="
//				+ URLEncoder.encode(filename , "UTF-8"));

//        if (dataMap.isEmpty()) {
//            return AjaxResult.error("数据为空");
//        }

        XSSFWorkbook workbook = new XSSFWorkbook();

        CellStyle borderStyle = workbook.createCellStyle();
        borderStyle.setBorderTop(BorderStyle.THIN);
        borderStyle.setBorderBottom(BorderStyle.THIN);
        borderStyle.setBorderLeft(BorderStyle.THIN);
        borderStyle.setBorderRight(BorderStyle.THIN);

        for (Map.Entry<String, Object> entry : dataMap.entrySet()) {
            XSSFSheet sheet = workbook.createSheet(entry.getKey());
            XSSFRow row = null;
            XSSFCell cell = null;
            XSSFCellStyle setBorder = workbook.createCellStyle();
            setBorder.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());// 设置背景色
            setBorder.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            int count = 10;
            if (headers != null) {
                count = headers.length;
                if (headers != null && count > 0) {
                    row = sheet.createRow(0);
                    for (int i = 0; i < count; i++) {
                        sheet.setColumnWidth(i, 15*256);
                        cell = row.createCell((short) i);
                        String desc = headers[i];
                        cell.setCellValue(new XSSFRichTextString(desc));
                        cell.setCellStyle(setBorder);
                    }
                }
            }
            String[][] rows = (String[][]) dataMap.get(entry.getKey());
            if (rows != null) {
                int len = rows.length;
                int pointer = 1;
                if (rows != null && len > 0) {
                    for (int i = 0; i < len; i++) {
                        row = sheet.createRow(i+1);
                        if(i>1&&rows[i][0]!=null&&rows[i-1][0]!=null&& !StringUtils.equals(rows[i][0],rows[i-1][0])){
                            CellRangeAddress cellAddresses = new CellRangeAddress(pointer,i,0,0);
                            sheet.addMergedRegion(cellAddresses);
                            pointer = i+1;
                        }else if(i==len-1){
                            CellRangeAddress cellAddresses = new CellRangeAddress(pointer,i+1,0,0);
                            sheet.addMergedRegion(cellAddresses);
                        }
                        for (int n = 0; n < count; n++) {
                            cell = row.createCell((short) n);
                            cell.setCellStyle(borderStyle);
                            if(n==0){
                                XSSFCellStyle style = workbook.createCellStyle();
                                style.setWrapText(true);
                                style.setAlignment(HorizontalAlignment.CENTER);
                                style.setVerticalAlignment(VerticalAlignment.CENTER);
                                style.setBorderTop(BorderStyle.THIN);
                                style.setBorderBottom(BorderStyle.THIN);
                                cell.setCellStyle(style);
                            }
                            Object value = rows[i][n];
                            if (value == null) {
                                cell.setCellValue(new XSSFRichTextString(""));
                                continue;
                            }
                            String rtype = value.getClass().getName();
                            if ("java.lang.String".equals(rtype)) {
                                cell.setCellValue(new XSSFRichTextString(value
                                        .toString()));
                            } else if ("java.lang.Integer".equals(rtype)) {
                                cell.setCellValue((Integer) value);
                            } else if ("java.lang.Double".equals(rtype)) {
                                cell.setCellValue((Double) value);
                            } else if ("java.lang.Long".equals(rtype)) {
                                cell.setCellValue((Long) value);
                            }
                        }
                    }
                }
            }
        }

        OutputStream output = null;
        try {
            output = response.getOutputStream();
            workbook.write(output);
            output.flush();
        } catch (IOException e) {
//                log.error("buildExcelDocument", e);
        } finally {
            if (output != null) {
                try {
                    output.close();
                    //workbook.close();
                } catch (IOException e) {
//                        log.error("buildExcelDocument", e);
                }
            }

        }
//        return AjaxResult.success(filename);
    }


    @Log(title = "vlan宽带设备", businessType = BusinessType.IMPORT)
    @RequiresPermissions("manage:vlanDevice:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        InputStream in = file.getInputStream();
        try {
            XSSFWorkbook wb = new XSSFWorkbook(in);

            int sheetNum = wb.getNumberOfSheets();
            for (int n=0; n<sheetNum; n++){
//                Long areaId ;
//                ManVlanArea areaParam = new ManVlanArea();
                XSSFSheet sheet = wb.getSheetAt(n);
//                areaParam.setAreaName(sheet.getSheetName());
//                List<ManVlanArea> areaParamList = manVlanAreaService.selectManVlanAreaList(areaParam);
//                if(areaParamList.size()==0){
//                    areaParam.setParentId(1L);
//                    manVlanAreaService.insertManVlanArea(areaParam);
//                    areaId = areaParam.getAreaId();
//                }else {
//                    areaId = areaParamList.get(0).getAreaId();
//                }
                List<ManVlanPon> ponList = new ArrayList<>();
                //默认第一行为标题行，i = 0
                for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                    XSSFRow row = sheet.getRow(i);
                    if(row==null) continue;
                    // 是否是合并单元格
                    boolean isMerge = isMergedRegion(sheet, i, 0);
                    String deviceName = "";
                    if (isMerge) {
                        deviceName = getMergedRegionValue(sheet, row.getRowNum(), 0);
                    } else {
                        deviceName = getCellValue(row.getCell(0));
                    }
                    Long deviceId ;
                    ManVlanDevice deviceParam = new ManVlanDevice();
                    deviceParam.setLoopBack(deviceName);
//                    deviceParam.setAreaId(areaId);
                    ManVlanDevice device = manVlanDeviceService.selectManVlanDevice(deviceParam);
                    if(device==null){
                        continue;
                    }

                    deviceId = device.getDeviceId();

                    ManVlanPon pon = new ManVlanPon();
                    pon.setDeviceId(deviceId);
                    pon.setDeviceName(deviceName);
                    pon.setSlot(getCellValue(row.getCell(1)));
                    pon.setPonName(getCellValue(row.getCell(2)));
                    pon.setPonPath(getCellValue(row.getCell(3)));
                    pon.setBusinessType(getCellValue(row.getCell(4)));
                    pon.setOutsideVlan(getCellValue(row.getCell(5)));
                    pon.setInnerVlan(getCellValue(row.getCell(6)));
                    pon.setClassifyDate(getCellValue(row.getCell(7)));
                    pon.setDelFlag("0");
                    ponList.add(pon);
                    //如果到了1000条直接插入  清空列表重新新增
                    if(ponList.size()==1000){
                        manVlanPonService.insertBatch(ponList);
                        ponList.clear();
                    }
                }
                if(ponList.size()>0) manVlanPonService.insertBatch(ponList);
            }
        } catch (Exception e) {
            throw new UtilException(e.getMessage());
        } finally {
            IOUtils.closeQuietly(in);
        }
        return AjaxResult.success("导入成功");
    }


    /**
     * 获取单元格的值
     * @param cell
     * @return
     */
    public static String getCellValue(Cell cell) {
        if (cell == null)
            return null;
        String retValue = null;
        switch (cell.getCellType()) {
            case NUMERIC: //数字
                retValue = String.valueOf(cell.getNumericCellValue()).trim();
                break;
            case STRING: //字符串
                retValue = cell.getStringCellValue().trim();
                break;
            case BOOLEAN: //Boolean
                retValue = String.valueOf(cell.getBooleanCellValue()).trim();
                break;
            case FORMULA: //公式
                retValue = String.valueOf(cell.getNumericCellValue()).trim();
                break;
            case BLANK://空值
                retValue = "";
                break;
            case ERROR://故障
                break;
            default:
                break;
        }
        return retValue;
    }

    /**
     * 判断指定的单元格是否是合并单元格
     *
     * @param sheet
     * @param row    行下标
     * @param column 列下标
     * @return
     */
    private static boolean isMergedRegion(Sheet sheet, int row, int column) {
        //获取该sheet所有合并的单元格
        int sheetMergeCount = sheet.getNumMergedRegions();
        //循环判断 该单元格属于哪个合并单元格， 如果能找到对应的，就表示该单元格是合并单元格
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstColumn = range.getFirstColumn();
            int lastColumn = range.getLastColumn();
            int firstRow = range.getFirstRow();
            int lastRow = range.getLastRow();
            if (row >= firstRow && row <= lastRow) {
                if (column >= firstColumn && column <= lastColumn) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * 获取合并单元格的值
     *
     * @param sheet  sheet索引 从0开始
     * @param row    行索引 从0开始
     * @param column 列索引  从0开始
     * @return
     */
    public static String getMergedRegionValue(Sheet sheet, int row, int column) {
        int sheetMergeCount = sheet.getNumMergedRegions();
        for (int i = 0; i < sheetMergeCount; i++) {
            CellRangeAddress ca = sheet.getMergedRegion(i);
            int firstColumn = ca.getFirstColumn();
            int lastColumn = ca.getLastColumn();
            int firstRow = ca.getFirstRow();
            int lastRow = ca.getLastRow();
            if (row >= firstRow && row <= lastRow) {
                if (column >= firstColumn && column <= lastColumn) {
                    Row fRow = sheet.getRow(firstRow);
                    Cell fCell = fRow.getCell(firstColumn);
                    return getCellValue(fCell);
                }
            }
        }
        return null;
    }
}
