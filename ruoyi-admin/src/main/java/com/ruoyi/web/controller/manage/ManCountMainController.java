package com.ruoyi.web.controller.manage;

import java.math.BigDecimal;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.manage.domain.ManCountMain;
import com.ruoyi.manage.service.IManCountMainService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 主要业务统计数据Controller
 * 
 * @author LR
 * @date 2024-02-22
 */
@Controller
@RequestMapping("/manage/countMain")
public class ManCountMainController extends BaseController
{
    private String prefix = "manage/countMain";

    @Autowired
    private IManCountMainService manCountMainService;

    @RequiresPermissions("manage:countMain:view")
    @GetMapping()
    public String countMain()
    {
        return prefix + "/countMain";
    }

    /**
     * 查询主要业务统计数据列表
     */
    @RequiresPermissions("manage:countMain:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ManCountMain manCountMain)
    {
        startPage();
        List<ManCountMain> list = manCountMainService.selectManCountMainList(manCountMain);
        return getDataTable(list);
    }

    /**
     * 查询主要业务统计数据列表不分页
     */
    @RequiresPermissions("manage:countMain:list")
    @PostMapping("/listNoPage")
    @ResponseBody
    public TableDataInfo listNoPage(ManCountMain manCountMain)
    {
        List<ManCountMain> list = manCountMainService.selectManCountMainList(manCountMain);
        if(list.size()>0){
            for (ManCountMain main : list){
                if(main.getZqzUserNumber()!=null&&main.getXgponDkzyNumber()!=null){
                    BigDecimal a = BigDecimal.valueOf(main.getZqzUserNumber());
                    BigDecimal b = BigDecimal.valueOf(main.getXgponDkzyNumber());
                    BigDecimal c = a.divide(b,2,BigDecimal.ROUND_HALF_UP);
                    main.setZqzVsXgpondk(c.toString());
                }
                if(main.getAddressAllNumber()!=null&&main.getAddressXgponNumber()!=null){
                    BigDecimal a = BigDecimal.valueOf(main.getAddressXgponNumber());
                    BigDecimal b = BigDecimal.valueOf(main.getAddressAllNumber());
                    BigDecimal c = a.divide(b,4,BigDecimal.ROUND_HALF_UP);
                    main.setZhanbi(c.multiply(new BigDecimal(100)).setScale(2)+"%");
                }
            }
        }
        return getDataTable(list);
    }

    /**
     * 导出主要业务统计数据列表
     */
    @RequiresPermissions("manage:countMain:export")
    @Log(title = "主要业务统计数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ManCountMain manCountMain)
    {
        List<ManCountMain> list = manCountMainService.selectManCountMainList(manCountMain);
        ExcelUtil<ManCountMain> util = new ExcelUtil<ManCountMain>(ManCountMain.class);
        return util.exportExcel(list, "主要业务统计数据数据");
    }

    /**
     * 新增主要业务统计数据
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存主要业务统计数据
     */
    @RequiresPermissions("manage:countMain:add")
    @Log(title = "主要业务统计数据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ManCountMain manCountMain)
    {
        return toAjax(manCountMainService.insertManCountMain(manCountMain));
    }

    /**
     * 修改主要业务统计数据
     */
    @RequiresPermissions("manage:countMain:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ManCountMain manCountMain = manCountMainService.selectManCountMainById(id);
        mmap.put("manCountMain", manCountMain);
        return prefix + "/edit";
    }

    /**
     * 修改保存主要业务统计数据
     */
    @RequiresPermissions("manage:countMain:edit")
    @Log(title = "主要业务统计数据", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ManCountMain manCountMain)
    {
        return toAjax(manCountMainService.updateManCountMain(manCountMain));
    }

    /**
     * 删除主要业务统计数据
     */
    @RequiresPermissions("manage:countMain:remove")
    @Log(title = "主要业务统计数据", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(manCountMainService.deleteManCountMainByIds(ids));
    }
}
