package com.ruoyi.manage.mapper;

import java.util.List;
import com.ruoyi.manage.domain.ManVlanPon;

/**
 * vlan宽带PON管理Mapper接口
 * 
 * @author LR
 * @date 2024-02-20
 */
public interface ManVlanPonMapper 
{
    /**
     * 查询vlan宽带PON管理
     * 
     * @param ponId vlan宽带PON管理主键
     * @return vlan宽带PON管理
     */
    public ManVlanPon selectManVlanPonByPonId(Long ponId);

    /**
     * 查询vlan宽带PON管理列表
     * 
     * @param manVlanPon vlan宽带PON管理
     * @return vlan宽带PON管理集合
     */
    public List<ManVlanPon> selectManVlanPonList(ManVlanPon manVlanPon);

    /**
     * 新增vlan宽带PON管理
     * 
     * @param manVlanPon vlan宽带PON管理
     * @return 结果
     */
    public int insertManVlanPon(ManVlanPon manVlanPon);

    /**
     * 修改vlan宽带PON管理
     * 
     * @param manVlanPon vlan宽带PON管理
     * @return 结果
     */
    public int updateManVlanPon(ManVlanPon manVlanPon);

    /**
     * 删除vlan宽带PON管理
     * 
     * @param ponId vlan宽带PON管理主键
     * @return 结果
     */
    public int deleteManVlanPonByPonId(Long ponId);

    /**
     * 批量删除vlan宽带PON管理
     * 
     * @param ponIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteManVlanPonByPonIds(String[] ponIds);

    /**
     * 查询vlan宽带PON管理列表
     *
     * @param manVlanPon vlan宽带PON管理
     * @return vlan宽带PON管理集合
     */
    public List<ManVlanPon> selectPonListWithParent(ManVlanPon manVlanPon);


    public void insertBatch(List<ManVlanPon> manVlanPonList);
}
