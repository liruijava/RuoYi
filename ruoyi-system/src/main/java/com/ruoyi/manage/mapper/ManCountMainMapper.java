package com.ruoyi.manage.mapper;

import java.util.List;
import com.ruoyi.manage.domain.ManCountMain;

/**
 * 主要业务统计数据Mapper接口
 * 
 * @author LR
 * @date 2024-02-22
 */
public interface ManCountMainMapper 
{
    /**
     * 查询主要业务统计数据
     * 
     * @param id 主要业务统计数据主键
     * @return 主要业务统计数据
     */
    public ManCountMain selectManCountMainById(Long id);

    /**
     * 查询主要业务统计数据列表
     * 
     * @param manCountMain 主要业务统计数据
     * @return 主要业务统计数据集合
     */
    public List<ManCountMain> selectManCountMainList(ManCountMain manCountMain);

    /**
     * 新增主要业务统计数据
     * 
     * @param manCountMain 主要业务统计数据
     * @return 结果
     */
    public int insertManCountMain(ManCountMain manCountMain);

    /**
     * 修改主要业务统计数据
     * 
     * @param manCountMain 主要业务统计数据
     * @return 结果
     */
    public int updateManCountMain(ManCountMain manCountMain);

    /**
     * 删除主要业务统计数据
     * 
     * @param id 主要业务统计数据主键
     * @return 结果
     */
    public int deleteManCountMainById(Long id);

    /**
     * 批量删除主要业务统计数据
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteManCountMainByIds(String[] ids);
}
