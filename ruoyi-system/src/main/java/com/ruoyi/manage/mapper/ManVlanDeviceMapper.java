package com.ruoyi.manage.mapper;

import java.util.List;

import com.ruoyi.manage.domain.ManVlanDevice;

/**
 * vlan宽带设备Mapper接口
 * 
 * @author LR
 * @date 2024-02-20
 */
public interface ManVlanDeviceMapper 
{
    /**
     * 查询vlan宽带设备
     * 
     * @param deviceId vlan宽带设备主键
     * @return vlan宽带设备
     */
    public ManVlanDevice selectManVlanDeviceByDeviceId(Long deviceId);

    /**
     * 查询vlan宽带设备列表
     * 
     * @param manVlanDevice vlan宽带设备
     * @return vlan宽带设备集合
     */
    public List<ManVlanDevice> selectManVlanDeviceList(ManVlanDevice manVlanDevice);

    /**
     * 新增vlan宽带设备
     * 
     * @param manVlanDevice vlan宽带设备
     * @return 结果
     */
    public int insertManVlanDevice(ManVlanDevice manVlanDevice);

    /**
     * 修改vlan宽带设备
     * 
     * @param manVlanDevice vlan宽带设备
     * @return 结果
     */
    public int updateManVlanDevice(ManVlanDevice manVlanDevice);

    /**
     * 删除vlan宽带设备
     * 
     * @param deviceId vlan宽带设备主键
     * @return 结果
     */
    public int deleteManVlanDeviceByDeviceId(Long deviceId);

    /**
     * 批量删除vlan宽带设备
     * 
     * @param deviceIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteManVlanDeviceByDeviceIds(String[] deviceIds);

    /**
     * 根据条件查询vlan宽带设备
     *
     * @param manVlanDevice vlan宽带设备
     * @return vlan宽带设备
     */
    public ManVlanDevice selectManVlanDevice(ManVlanDevice manVlanDevice);

}
