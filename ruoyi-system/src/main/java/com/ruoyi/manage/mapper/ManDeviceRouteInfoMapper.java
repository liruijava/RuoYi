package com.ruoyi.manage.mapper;

import java.util.List;
import com.ruoyi.manage.domain.ManDeviceRouteInfo;

/**
 * 设备路由信息Mapper接口
 * 
 * @author LR
 * @date 2024-03-26
 */
public interface ManDeviceRouteInfoMapper 
{
    /**
     * 查询设备路由信息
     * 
     * @param id 设备路由信息主键
     * @return 设备路由信息
     */
    public ManDeviceRouteInfo selectManDeviceRouteInfoById(Long id);

    /**
     * 查询设备路由信息列表
     * 
     * @param manDeviceRouteInfo 设备路由信息
     * @return 设备路由信息集合
     */
    public List<ManDeviceRouteInfo> selectManDeviceRouteInfoListOlt(ManDeviceRouteInfo manDeviceRouteInfo);
    public List<ManDeviceRouteInfo> selectManDeviceRouteInfoListSw(ManDeviceRouteInfo manDeviceRouteInfo);
    public List<ManDeviceRouteInfo> selectManDeviceRouteInfoListCr(ManDeviceRouteInfo manDeviceRouteInfo);

    /**
     * 新增设备路由信息
     * 
     * @param manDeviceRouteInfo 设备路由信息
     * @return 结果
     */
    public int insertManDeviceRouteInfo(ManDeviceRouteInfo manDeviceRouteInfo);

    /**
     * 修改设备路由信息
     * 
     * @param manDeviceRouteInfo 设备路由信息
     * @return 结果
     */
    public int updateManDeviceRouteInfo(ManDeviceRouteInfo manDeviceRouteInfo);

    /**
     * 删除设备路由信息
     * 
     * @param id 设备路由信息主键
     * @return 结果
     */
    public int deleteManDeviceRouteInfoById(Long id);

    /**
     * 批量删除设备路由信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteManDeviceRouteInfoByIds(String[] ids);

    public void insertBatch(List<ManDeviceRouteInfo> manDeviceRouteInfoList);
}
