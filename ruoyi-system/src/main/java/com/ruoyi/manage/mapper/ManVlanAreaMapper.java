package com.ruoyi.manage.mapper;

import com.ruoyi.manage.domain.ManVlanArea;

import java.util.List;

/**
 * vlan宽带规划区域Mapper接口
 * 
 * @author LR
 * @date 2024-02-19
 */
public interface ManVlanAreaMapper 
{
    /**
     * 查询vlan宽带规划区域
     * 
     * @param areaId vlan宽带规划区域主键
     * @return vlan宽带规划区域
     */
    public ManVlanArea selectManVlanAreaByAreaId(Long areaId);

    /**
     * 查询vlan宽带规划区域列表
     * 
     * @param manVlanArea vlan宽带规划区域
     * @return vlan宽带规划区域集合
     */
    public List<ManVlanArea> selectManVlanAreaList(ManVlanArea manVlanArea);

    /**
     * 新增vlan宽带规划区域
     * 
     * @param manVlanArea vlan宽带规划区域
     * @return 结果
     */
    public int insertManVlanArea(ManVlanArea manVlanArea);

    /**
     * 修改vlan宽带规划区域
     * 
     * @param manVlanArea vlan宽带规划区域
     * @return 结果
     */
    public int updateManVlanArea(ManVlanArea manVlanArea);

    /**
     * 删除vlan宽带规划区域
     * 
     * @param areaId vlan宽带规划区域主键
     * @return 结果
     */
    public int deleteManVlanAreaByAreaId(Long areaId);

    /**
     * 批量删除vlan宽带规划区域
     * 
     * @param areaIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteManVlanAreaByAreaIds(String[] areaIds);
}
