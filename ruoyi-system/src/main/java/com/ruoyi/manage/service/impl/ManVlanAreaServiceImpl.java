package com.ruoyi.manage.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.manage.domain.ManVlanArea;
import com.ruoyi.manage.domain.ManVlanPon;
import com.ruoyi.manage.mapper.ManVlanAreaMapper;
import com.ruoyi.manage.mapper.ManVlanPonMapper;
import com.ruoyi.manage.service.IManVlanAreaService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.core.text.Convert;

/**
 * vlan宽带规划区域Service业务层处理
 * 
 * @author LR
 * @date 2024-02-19
 */
@Service
public class ManVlanAreaServiceImpl implements IManVlanAreaService
{
    @Autowired
    private ManVlanAreaMapper manVlanAreaMapper;

    @Autowired
    private ManVlanPonMapper manVlanPonMapper;

    /**
     * 查询vlan宽带规划区域
     * 
     * @param areaId vlan宽带规划区域主键
     * @return vlan宽带规划区域
     */
    @Override
    public ManVlanArea selectManVlanAreaByAreaId(Long areaId)
    {
        return manVlanAreaMapper.selectManVlanAreaByAreaId(areaId);
    }

    /**
     * 查询vlan宽带规划区域列表
     * 
     * @param manVlanArea vlan宽带规划区域
     * @return vlan宽带规划区域
     */
    @Override
    public List<ManVlanArea> selectManVlanAreaList(ManVlanArea manVlanArea)
    {
        return manVlanAreaMapper.selectManVlanAreaList(manVlanArea);
    }

    /**
     * 新增vlan宽带规划区域
     * 
     * @param manVlanArea vlan宽带规划区域
     * @return 结果
     */
    @Override
    public int insertManVlanArea(ManVlanArea manVlanArea)
    {
        if(manVlanArea.getParentId()!=null){
            ManVlanArea info = manVlanAreaMapper.selectManVlanAreaByAreaId(manVlanArea.getParentId());
            // 如果父节点不为"正常"状态,则不允许新增子节点
            if (!UserConstants.DEPT_NORMAL.equals(info.getStatus()))
            {
                throw new ServiceException("区域停用，不允许新增");
            }
            manVlanArea.setAncestors(info.getAncestors() + "," + manVlanArea.getParentId());
        }
        manVlanArea.setCreateTime(DateUtils.getNowDate());
        return manVlanAreaMapper.insertManVlanArea(manVlanArea);
    }

    /**
     * 修改vlan宽带规划区域
     * 
     * @param manVlanArea vlan宽带规划区域
     * @return 结果
     */
    @Override
    public int updateManVlanArea(ManVlanArea manVlanArea)
    {
        manVlanArea.setUpdateTime(DateUtils.getNowDate());
        return manVlanAreaMapper.updateManVlanArea(manVlanArea);
    }

    /**
     * 批量删除vlan宽带规划区域
     * 
     * @param areaIds 需要删除的vlan宽带规划区域主键
     * @return 结果
     */
    @Override
    public int deleteManVlanAreaByAreaIds(String areaIds)
    {
        return manVlanAreaMapper.deleteManVlanAreaByAreaIds(Convert.toStrArray(areaIds));
    }

    /**
     * 删除vlan宽带规划区域信息
     * 
     * @param areaId vlan宽带规划区域主键
     * @return 结果
     */
    @Override
    public int deleteManVlanAreaByAreaId(Long areaId)
    {
        return manVlanAreaMapper.deleteManVlanAreaByAreaId(areaId);
    }

    /**
     * 查询部门管理树（排除下级）
     *
     * @return 所有部门信息
     */
    @Override
    public List<Ztree> selectAreaTreeExcludeChild(ManVlanArea manVlanArea)
    {
        Long excludeId = manVlanArea.getExcludeId();
        List<ManVlanArea> areaList = manVlanAreaMapper.selectManVlanAreaList(manVlanArea);
        if (excludeId.intValue() > 0)
        {
            areaList.removeIf(d -> d.getAreaId().intValue() == excludeId || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), excludeId + ""));
        }
        List<Ztree> ztrees = initZtree(areaList);
        return ztrees;
    }

    /**
     * 对象转区域树
     *
     * @param areaList 区域列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<ManVlanArea> areaList)
    {
        return initZtree(areaList, null);
    }

    /**
     * 对象转区域树
     *
     * @param areaList 区域列表
     * @param roleAreaList 已存在区域列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<ManVlanArea> areaList, List<String> roleAreaList)
    {

        List<Ztree> ztrees = new ArrayList<Ztree>();
        boolean isCheck = StringUtils.isNotNull(roleAreaList);
        for (ManVlanArea area : areaList)
        {
            if (UserConstants.DEPT_NORMAL.equals(area.getStatus()))
            {
                Ztree ztree = new Ztree();
                ztree.setId(area.getAreaId());
                ztree.setpId(area.getParentId());
                ztree.setName(area.getAreaName());
                ztree.setTitle(area.getAreaName());
                if (isCheck)
                {
                    ztree.setChecked(roleAreaList.contains(area.getAreaId() + area.getAreaName()));
                }
                ztrees.add(ztree);
            }
        }
        return ztrees;
    }

    /**
     * 查询区域管理树
     *
     * @param manVlanArea 区域信息
     * @return 所有区域信息
     */
    @Override
    public List<Ztree> selectVlanAreaTree(ManVlanArea manVlanArea)
    {
        List<ManVlanArea> areaList = manVlanAreaMapper.selectManVlanAreaList(manVlanArea);
        List<Ztree> ztrees = initZtree(areaList);
        return ztrees;
    }

    /**
     * 查询导出数据
     *
     * @param manVlanArea 区域信息
     * @return 所有区域信息
     */
    @Override
    public List<ManVlanArea> selectAreaListWithDevice(ManVlanArea manVlanArea) {
        List<ManVlanArea> areaList = new ArrayList<>();
        List<ManVlanPon> ponList = manVlanPonMapper.selectPonListWithParent(new ManVlanPon());
        if(ponList.size()>0){
            Map<String, List<ManVlanPon>> groupMap = ponList.stream()
                    .collect(Collectors.groupingBy(ManVlanPon::getAreaName));
            groupMap.forEach((k, v) -> {
                ManVlanArea area = new ManVlanArea();
                area.setAreaName(k);
                area.setPonList(v);
                areaList.add(area);
            });
        }
        return areaList;
    }

}
