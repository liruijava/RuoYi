package com.ruoyi.manage.service;

import java.util.List;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.manage.domain.ManVlanArea;

/**
 * vlan宽带规划区域Service接口
 * 
 * @author LR
 * @date 2024-02-19
 */
public interface IManVlanAreaService 
{
    /**
     * 查询vlan宽带规划区域
     * 
     * @param areaId vlan宽带规划区域主键
     * @return vlan宽带规划区域
     */
    public ManVlanArea selectManVlanAreaByAreaId(Long areaId);

    /**
     * 查询vlan宽带规划区域列表
     * 
     * @param manVlanArea vlan宽带规划区域
     * @return vlan宽带规划区域集合
     */
    public List<ManVlanArea> selectManVlanAreaList(ManVlanArea manVlanArea);

    /**
     * 新增vlan宽带规划区域
     * 
     * @param manVlanArea vlan宽带规划区域
     * @return 结果
     */
    public int insertManVlanArea(ManVlanArea manVlanArea);

    /**
     * 修改vlan宽带规划区域
     * 
     * @param manVlanArea vlan宽带规划区域
     * @return 结果
     */
    public int updateManVlanArea(ManVlanArea manVlanArea);

    /**
     * 批量删除vlan宽带规划区域
     * 
     * @param areaIds 需要删除的vlan宽带规划区域主键集合
     * @return 结果
     */
    public int deleteManVlanAreaByAreaIds(String areaIds);

    /**
     * 删除vlan宽带规划区域信息
     * 
     * @param areaId vlan宽带规划区域主键
     * @return 结果
     */
    public int deleteManVlanAreaByAreaId(Long areaId);

    /**
     * 查询部门管理树（排除下级）
     *
     * @param manVlanArea 部门信息
     * @return 所有部门信息
     */
    public List<Ztree> selectAreaTreeExcludeChild(ManVlanArea manVlanArea);

    /**
     * 查询区域管理树
     *
     * @param manVlanArea 区域信息
     * @return 所有区域信息
     */
    public List<Ztree> selectVlanAreaTree(ManVlanArea manVlanArea);

    /**
     * 查询导出数据
     *
     * @param manVlanArea 区域信息
     * @return 所有区域信息
     */
    public List<ManVlanArea> selectAreaListWithDevice(ManVlanArea manVlanArea);
    
}
