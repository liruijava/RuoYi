package com.ruoyi.manage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.ManVlanPonMapper;
import com.ruoyi.manage.domain.ManVlanPon;
import com.ruoyi.manage.service.IManVlanPonService;
import com.ruoyi.common.core.text.Convert;

/**
 * vlan宽带PON管理Service业务层处理
 * 
 * @author LR
 * @date 2024-02-20
 */
@Service
public class ManVlanPonServiceImpl implements IManVlanPonService 
{
    @Autowired
    private ManVlanPonMapper manVlanPonMapper;

    /**
     * 查询vlan宽带PON管理
     * 
     * @param ponId vlan宽带PON管理主键
     * @return vlan宽带PON管理
     */
    @Override
    public ManVlanPon selectManVlanPonByPonId(Long ponId)
    {
        return manVlanPonMapper.selectManVlanPonByPonId(ponId);
    }

    /**
     * 查询vlan宽带PON管理列表
     * 
     * @param manVlanPon vlan宽带PON管理
     * @return vlan宽带PON管理
     */
    @Override
    public List<ManVlanPon> selectManVlanPonList(ManVlanPon manVlanPon)
    {
        return manVlanPonMapper.selectManVlanPonList(manVlanPon);
    }

    /**
     * 新增vlan宽带PON管理
     * 
     * @param manVlanPon vlan宽带PON管理
     * @return 结果
     */
    @Override
    public int insertManVlanPon(ManVlanPon manVlanPon)
    {
        manVlanPon.setCreateTime(DateUtils.getNowDate());
        return manVlanPonMapper.insertManVlanPon(manVlanPon);
    }

    /**
     * 修改vlan宽带PON管理
     * 
     * @param manVlanPon vlan宽带PON管理
     * @return 结果
     */
    @Override
    public int updateManVlanPon(ManVlanPon manVlanPon)
    {
        manVlanPon.setUpdateTime(DateUtils.getNowDate());
        return manVlanPonMapper.updateManVlanPon(manVlanPon);
    }

    /**
     * 批量删除vlan宽带PON管理
     * 
     * @param ponIds 需要删除的vlan宽带PON管理主键
     * @return 结果
     */
    @Override
    public int deleteManVlanPonByPonIds(String ponIds)
    {
        return manVlanPonMapper.deleteManVlanPonByPonIds(Convert.toStrArray(ponIds));
    }

    /**
     * 删除vlan宽带PON管理信息
     * 
     * @param ponId vlan宽带PON管理主键
     * @return 结果
     */
    @Override
    public int deleteManVlanPonByPonId(Long ponId)
    {
        return manVlanPonMapper.deleteManVlanPonByPonId(ponId);
    }

    @Override
    public void insertBatch(List<ManVlanPon> manVlanPonList){
      manVlanPonMapper.insertBatch(manVlanPonList);
    }
}
