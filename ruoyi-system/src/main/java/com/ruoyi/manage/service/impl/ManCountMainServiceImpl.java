package com.ruoyi.manage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.ManCountMainMapper;
import com.ruoyi.manage.domain.ManCountMain;
import com.ruoyi.manage.service.IManCountMainService;
import com.ruoyi.common.core.text.Convert;

/**
 * 主要业务统计数据Service业务层处理
 * 
 * @author LR
 * @date 2024-02-22
 */
@Service
public class ManCountMainServiceImpl implements IManCountMainService 
{
    @Autowired
    private ManCountMainMapper manCountMainMapper;

    /**
     * 查询主要业务统计数据
     * 
     * @param id 主要业务统计数据主键
     * @return 主要业务统计数据
     */
    @Override
    public ManCountMain selectManCountMainById(Long id)
    {
        return manCountMainMapper.selectManCountMainById(id);
    }

    /**
     * 查询主要业务统计数据列表
     * 
     * @param manCountMain 主要业务统计数据
     * @return 主要业务统计数据
     */
    @Override
    public List<ManCountMain> selectManCountMainList(ManCountMain manCountMain)
    {
        return manCountMainMapper.selectManCountMainList(manCountMain);
    }

    /**
     * 新增主要业务统计数据
     * 
     * @param manCountMain 主要业务统计数据
     * @return 结果
     */
    @Override
    public int insertManCountMain(ManCountMain manCountMain)
    {
        manCountMain.setCreateTime(DateUtils.getNowDate());
        return manCountMainMapper.insertManCountMain(manCountMain);
    }

    /**
     * 修改主要业务统计数据
     * 
     * @param manCountMain 主要业务统计数据
     * @return 结果
     */
    @Override
    public int updateManCountMain(ManCountMain manCountMain)
    {
        manCountMain.setUpdateTime(DateUtils.getNowDate());
        return manCountMainMapper.updateManCountMain(manCountMain);
    }

    /**
     * 批量删除主要业务统计数据
     * 
     * @param ids 需要删除的主要业务统计数据主键
     * @return 结果
     */
    @Override
    public int deleteManCountMainByIds(String ids)
    {
        return manCountMainMapper.deleteManCountMainByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除主要业务统计数据信息
     * 
     * @param id 主要业务统计数据主键
     * @return 结果
     */
    @Override
    public int deleteManCountMainById(Long id)
    {
        return manCountMainMapper.deleteManCountMainById(id);
    }
}
