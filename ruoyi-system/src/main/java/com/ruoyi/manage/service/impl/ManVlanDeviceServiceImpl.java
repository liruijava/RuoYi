package com.ruoyi.manage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.manage.domain.ManVlanDevice;
import com.ruoyi.manage.mapper.ManVlanDeviceMapper;
import com.ruoyi.manage.service.IManVlanDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.core.text.Convert;

/**
 * vlan宽带设备Service业务层处理
 * 
 * @author LR
 * @date 2024-02-20
 */
@Service
public class ManVlanDeviceServiceImpl implements IManVlanDeviceService {
    @Autowired
    private ManVlanDeviceMapper manVlanDeviceMapper;

    /**
     * 查询vlan宽带设备
     * 
     * @param deviceId vlan宽带设备主键
     * @return vlan宽带设备
     */
    @Override
    public ManVlanDevice selectManVlanDeviceByDeviceId(Long deviceId)
    {
        return manVlanDeviceMapper.selectManVlanDeviceByDeviceId(deviceId);
    }

    /**
     * 查询vlan宽带设备列表
     * 
     * @param manVlanDevice vlan宽带设备
     * @return vlan宽带设备
     */
    @Override
    public List<ManVlanDevice> selectManVlanDeviceList(ManVlanDevice manVlanDevice)
    {
        return manVlanDeviceMapper.selectManVlanDeviceList(manVlanDevice);
    }

    /**
     * 新增vlan宽带设备
     * 
     * @param manVlanDevice vlan宽带设备
     * @return 结果
     */
    @Override
    public int insertManVlanDevice(ManVlanDevice manVlanDevice)
    {
        manVlanDevice.setCreateTime(DateUtils.getNowDate());
        return manVlanDeviceMapper.insertManVlanDevice(manVlanDevice);
    }

    /**
     * 修改vlan宽带设备
     * 
     * @param manVlanDevice vlan宽带设备
     * @return 结果
     */
    @Override
    public int updateManVlanDevice(ManVlanDevice manVlanDevice)
    {
        manVlanDevice.setUpdateTime(DateUtils.getNowDate());
        return manVlanDeviceMapper.updateManVlanDevice(manVlanDevice);
    }

    /**
     * 批量删除vlan宽带设备
     * 
     * @param deviceIds 需要删除的vlan宽带设备主键
     * @return 结果
     */
    @Override
    public int deleteManVlanDeviceByDeviceIds(String deviceIds)
    {
        return manVlanDeviceMapper.deleteManVlanDeviceByDeviceIds(Convert.toStrArray(deviceIds));
    }

    /**
     * 删除vlan宽带设备信息
     * 
     * @param deviceId vlan宽带设备主键
     * @return 结果
     */
    @Override
    public int deleteManVlanDeviceByDeviceId(Long deviceId)
    {
        return manVlanDeviceMapper.deleteManVlanDeviceByDeviceId(deviceId);
    }

    /**
     * 根据条件查询vlan宽带设备
     *
     * @param manVlanDevice vlan宽带设备
     * @return vlan宽带设备
     */
    @Override
    public ManVlanDevice selectManVlanDevice(ManVlanDevice manVlanDevice)
    {
        return manVlanDeviceMapper.selectManVlanDevice(manVlanDevice);
    }

}
