package com.ruoyi.manage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.manage.mapper.ManDeviceRouteInfoMapper;
import com.ruoyi.manage.domain.ManDeviceRouteInfo;
import com.ruoyi.manage.service.IManDeviceRouteInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 设备路由信息Service业务层处理
 * 
 * @author LR
 * @date 2024-03-26
 */
@Service
public class ManDeviceRouteInfoServiceImpl implements IManDeviceRouteInfoService 
{
    @Autowired
    private ManDeviceRouteInfoMapper manDeviceRouteInfoMapper;

    /**
     * 查询设备路由信息
     * 
     * @param id 设备路由信息主键
     * @return 设备路由信息
     */
    @Override
    public ManDeviceRouteInfo selectManDeviceRouteInfoById(Long id)
    {
        return manDeviceRouteInfoMapper.selectManDeviceRouteInfoById(id);
    }

    /**
     * 查询设备路由信息列表
     * 
     * @param manDeviceRouteInfo 设备路由信息
     * @return 设备路由信息
     */
    @Override
    public List<ManDeviceRouteInfo> selectManDeviceRouteInfoListOlt(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        return manDeviceRouteInfoMapper.selectManDeviceRouteInfoListOlt(manDeviceRouteInfo);
    }
    @Override
    public List<ManDeviceRouteInfo> selectManDeviceRouteInfoListSw(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        return manDeviceRouteInfoMapper.selectManDeviceRouteInfoListSw(manDeviceRouteInfo);
    }
    @Override
    public List<ManDeviceRouteInfo> selectManDeviceRouteInfoListCr(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        return manDeviceRouteInfoMapper.selectManDeviceRouteInfoListCr(manDeviceRouteInfo);
    }

    /**
     * 新增设备路由信息
     * 
     * @param manDeviceRouteInfo 设备路由信息
     * @return 结果
     */
    @Override
    public int insertManDeviceRouteInfo(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        manDeviceRouteInfo.setCreateTime(DateUtils.getNowDate());
        return manDeviceRouteInfoMapper.insertManDeviceRouteInfo(manDeviceRouteInfo);
    }

    /**
     * 修改设备路由信息
     * 
     * @param manDeviceRouteInfo 设备路由信息
     * @return 结果
     */
    @Override
    public int updateManDeviceRouteInfo(ManDeviceRouteInfo manDeviceRouteInfo)
    {
        manDeviceRouteInfo.setUpdateTime(DateUtils.getNowDate());
        return manDeviceRouteInfoMapper.updateManDeviceRouteInfo(manDeviceRouteInfo);
    }

    /**
     * 批量删除设备路由信息
     * 
     * @param ids 需要删除的设备路由信息主键
     * @return 结果
     */
    @Override
    public int deleteManDeviceRouteInfoByIds(String ids)
    {
        return manDeviceRouteInfoMapper.deleteManDeviceRouteInfoByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除设备路由信息信息
     * 
     * @param id 设备路由信息主键
     * @return 结果
     */
    @Override
    public int deleteManDeviceRouteInfoById(Long id)
    {
        return manDeviceRouteInfoMapper.deleteManDeviceRouteInfoById(id);
    }

    @Override
    public void insertBatch(List<ManDeviceRouteInfo> manDeviceRouteInfoList){
        manDeviceRouteInfoMapper.insertBatch(manDeviceRouteInfoList);
    }
}
