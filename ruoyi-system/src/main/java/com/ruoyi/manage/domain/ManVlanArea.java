package com.ruoyi.manage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * MSE/SW设备管理及区域对象 man_vlan_area
 * 
 * @author LR
 * @date 2024-03-20
 */
public class ManVlanArea extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 区域id */
    private Long areaId;

    /** 父区域id */
    @Excel(name = "父区域id")
    private Long parentId;

    /** 祖级列表 */
    @Excel(name = "祖级列表")
    private String ancestors;

    /** 区域/设备名称 */
    @Excel(name = "区域/设备名称")
    private String areaName;

    /** 区域/设备详细地址 */
    @Excel(name = "区域/设备详细地址")
    private String areaPath;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer sort;

    /** 数量 */
    @Excel(name = "数量")
    private Integer acount;

    /** 其他VLAN信息 */
    @Excel(name = "其他VLAN信息")
    private String otherData;

    /** 上联93链路 */
    @Excel(name = "上联93链路")
    private String upperNineThree;

    /** 上联MSE链路 */
    @Excel(name = "上联MSE链路")
    private String upperMse;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    /** 设备属性 */
    @Excel(name = "设备属性")
    private String deviceAttribute;

    /** 设备厂家 */
    @Excel(name = "设备厂家")
    private String deviceFactory;

    /** 设备型号 */
    @Excel(name = "设备型号")
    private String deviceModel;

    /** 网管名称 */
    @Excel(name = "网管名称")
    private String networkName;

    /** 设备类型 */
    @Excel(name = "设备类型")
    private String deviceType;

    /** LoopBack */
    @Excel(name = "LoopBack")
    private String loopBack;

    /** 局站名称 */
    @Excel(name = "局站名称")
    private String stationName;

    /** 局站位置 */
    @Excel(name = "局站位置")
    private String stationPath;

    public void setAreaId(Long areaId) 
    {
        this.areaId = areaId;
    }

    public Long getAreaId() 
    {
        return areaId;
    }
    public void setParentId(Long parentId) 
    {
        this.parentId = parentId;
    }

    public Long getParentId() 
    {
        return parentId;
    }
    public void setAncestors(String ancestors) 
    {
        this.ancestors = ancestors;
    }

    public String getAncestors() 
    {
        return ancestors;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setAreaPath(String areaPath) 
    {
        this.areaPath = areaPath;
    }

    public String getAreaPath() 
    {
        return areaPath;
    }
    public void setSort(Integer sort) 
    {
        this.sort = sort;
    }

    public Integer getSort() 
    {
        return sort;
    }
    public void setAcount(Integer acount) 
    {
        this.acount = acount;
    }

    public Integer getAcount() 
    {
        return acount;
    }
    public void setOtherData(String otherData) 
    {
        this.otherData = otherData;
    }

    public String getOtherData() 
    {
        return otherData;
    }
    public void setUpperNineThree(String upperNineThree) 
    {
        this.upperNineThree = upperNineThree;
    }

    public String getUpperNineThree() 
    {
        return upperNineThree;
    }
    public void setUpperMse(String upperMse) 
    {
        this.upperMse = upperMse;
    }

    public String getUpperMse() 
    {
        return upperMse;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setDeviceAttribute(String deviceAttribute) 
    {
        this.deviceAttribute = deviceAttribute;
    }

    public String getDeviceAttribute() 
    {
        return deviceAttribute;
    }
    public void setDeviceFactory(String deviceFactory) 
    {
        this.deviceFactory = deviceFactory;
    }

    public String getDeviceFactory() 
    {
        return deviceFactory;
    }
    public void setDeviceModel(String deviceModel) 
    {
        this.deviceModel = deviceModel;
    }

    public String getDeviceModel() 
    {
        return deviceModel;
    }
    public void setDeviceType(String deviceType) 
    {
        this.deviceType = deviceType;
    }

    public String getNetworkName() { return networkName; }
    public void setNetworkName(String networkName) { this.networkName = networkName; }

    public String getDeviceType()
    {
        return deviceType;
    }
    public void setLoopBack(String loopBack) 
    {
        this.loopBack = loopBack;
    }

    public String getLoopBack() 
    {
        return loopBack;
    }
    public void setStationName(String stationName) 
    {
        this.stationName = stationName;
    }

    public String getStationName() 
    {
        return stationName;
    }
    public void setStationPath(String stationPath) 
    {
        this.stationPath = stationPath;
    }

    public String getStationPath() 
    {
        return stationPath;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("areaId", getAreaId())
            .append("parentId", getParentId())
            .append("ancestors", getAncestors())
            .append("areaName", getAreaName())
            .append("areaPath", getAreaPath())
            .append("sort", getSort())
            .append("acount", getAcount())
            .append("otherData", getOtherData())
            .append("upperNineThree", getUpperNineThree())
            .append("upperMse", getUpperMse())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("deviceAttribute", getDeviceAttribute())
            .append("deviceFactory", getDeviceFactory())
            .append("deviceModel", getDeviceModel())
            .append("networkName", getNetworkName())
            .append("deviceType", getDeviceType())
            .append("loopBack", getLoopBack())
            .append("stationName", getStationName())
            .append("stationPath", getStationPath())
            .toString();
    }


    /**************************************虚拟字段****************************************/
    private String parentName;
    /** 排除编号 */
    private Long excludeId;

    private List<ManVlanPon> ponList;

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Long getExcludeId() {
        return excludeId;
    }

    public void setExcludeId(Long excludeId) {
        this.excludeId = excludeId;
    }

    public List<ManVlanPon> getPonList() {
        return ponList;
    }

    public void setPonList(List<ManVlanPon> ponList) {
        this.ponList = ponList;
    }
}
