package com.ruoyi.manage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * vlan宽带设备对象 man_vlan_device
 *
 * @author LR
 * @date 2024-03-21
 */
public class ManVlanDevice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 设备id */
    private Long deviceId;

    /** 父区域id */
    @Excel(name = "父区域id")
    private Long areaId;

    /** 祖级列表 */
    @Excel(name = "祖级列表")
    private String ancestors;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String deviceName;

    /** 设备详细地址 */
    @Excel(name = "设备详细地址")
    private String devicePath;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer sort;

    /** 数量 */
    @Excel(name = "数量")
    private Integer acount;

    /** 设备属性 */
    @Excel(name = "设备属性")
    private String deviceAttribute;

    /** 设备厂家 */
    @Excel(name = "设备厂家")
    private String deviceFactory;

    /** 设备型号 */
    @Excel(name = "设备型号")
    private String deviceModel;

    /** 网管名称 */
    @Excel(name = "网管名称")
    private String networkName;

    /** 设备类型 */
    @Excel(name = "设备类型")
    private String deviceType;

    /** LoopBack */
    @Excel(name = "LoopBack")
    private String loopBack;

    /** 局站名称 */
    @Excel(name = "局站名称")
    private String stationName;

    /** 局站位置 */
    @Excel(name = "局站位置")
    private String stationPath;

    /** 管理IP */
    @Excel(name = "管理IP")
    private String manageIp;

    /** 管理VLAN */
    @Excel(name = "管理VLAN")
    private String manageVlan;

    /** VLAN43 */
    @Excel(name = "VLAN43")
    private String vlanFourThree;

    /** VLAN45 */
    @Excel(name = "VLAN45")
    private String vlanFourFive;

    /** VLAN46 */
    @Excel(name = "VLAN46")
    private String vlanFourSix;

    /** 其他VLAN信息 */
    @Excel(name = "其他VLAN信息")
    private String otherData;

    /** 上联93链路 */
    @Excel(name = "上联93链路")
    private String upperNineThree;

    /** 上联MES链路 */
    @Excel(name = "上联MES链路")
    private String upperMes;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    public void setDeviceId(Long deviceId)
    {
        this.deviceId = deviceId;
    }

    public Long getDeviceId()
    {
        return deviceId;
    }
    public void setAreaId(Long areaId)
    {
        this.areaId = areaId;
    }

    public Long getAreaId()
    {
        return areaId;
    }
    public void setAncestors(String ancestors)
    {
        this.ancestors = ancestors;
    }

    public String getAncestors()
    {
        return ancestors;
    }
    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }

    public String getDeviceName()
    {
        return deviceName;
    }
    public void setDevicePath(String devicePath)
    {
        this.devicePath = devicePath;
    }

    public String getDevicePath()
    {
        return devicePath;
    }
    public void setSort(Integer sort)
    {
        this.sort = sort;
    }

    public Integer getSort()
    {
        return sort;
    }
    public void setAcount(Integer acount)
    {
        this.acount = acount;
    }

    public Integer getAcount()
    {
        return acount;
    }
    public void setDeviceAttribute(String deviceAttribute)
    {
        this.deviceAttribute = deviceAttribute;
    }

    public String getDeviceAttribute()
    {
        return deviceAttribute;
    }
    public void setDeviceFactory(String deviceFactory)
    {
        this.deviceFactory = deviceFactory;
    }

    public String getDeviceFactory()
    {
        return deviceFactory;
    }
    public void setDeviceModel(String deviceModel)
    {
        this.deviceModel = deviceModel;
    }

    public String getDeviceModel()
    {
        return deviceModel;
    }
    public void setNetworkName(String networkName)
    {
        this.networkName = networkName;
    }

    public String getNetworkName()
    {
        return networkName;
    }
    public void setDeviceType(String deviceType)
    {
        this.deviceType = deviceType;
    }

    public String getDeviceType()
    {
        return deviceType;
    }
    public void setLoopBack(String loopBack)
    {
        this.loopBack = loopBack;
    }

    public String getLoopBack()
    {
        return loopBack;
    }
    public void setStationName(String stationName)
    {
        this.stationName = stationName;
    }

    public String getStationName()
    {
        return stationName;
    }
    public void setStationPath(String stationPath)
    {
        this.stationPath = stationPath;
    }

    public String getStationPath()
    {
        return stationPath;
    }
    public void setManageIp(String manageIp)
    {
        this.manageIp = manageIp;
    }

    public String getManageIp()
    {
        return manageIp;
    }
    public void setManageVlan(String manageVlan)
    {
        this.manageVlan = manageVlan;
    }

    public String getManageVlan()
    {
        return manageVlan;
    }
    public void setVlanFourThree(String vlanFourThree)
    {
        this.vlanFourThree = vlanFourThree;
    }

    public String getVlanFourThree()
    {
        return vlanFourThree;
    }
    public void setVlanFourFive(String vlanFourFive)
    {
        this.vlanFourFive = vlanFourFive;
    }

    public String getVlanFourFive()
    {
        return vlanFourFive;
    }
    public void setVlanFourSix(String vlanFourSix)
    {
        this.vlanFourSix = vlanFourSix;
    }

    public String getVlanFourSix()
    {
        return vlanFourSix;
    }
    public void setOtherData(String otherData)
    {
        this.otherData = otherData;
    }

    public String getOtherData()
    {
        return otherData;
    }
    public void setUpperNineThree(String upperNineThree)
    {
        this.upperNineThree = upperNineThree;
    }

    public String getUpperNineThree()
    {
        return upperNineThree;
    }
    public void setUpperMes(String upperMes)
    {
        this.upperMes = upperMes;
    }

    public String getUpperMes()
    {
        return upperMes;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("deviceId", getDeviceId())
                .append("areaId", getAreaId())
                .append("ancestors", getAncestors())
                .append("deviceName", getDeviceName())
                .append("devicePath", getDevicePath())
                .append("sort", getSort())
                .append("acount", getAcount())
                .append("deviceAttribute", getDeviceAttribute())
                .append("deviceFactory", getDeviceFactory())
                .append("deviceModel", getDeviceModel())
                .append("networkName", getNetworkName())
                .append("deviceType", getDeviceType())
                .append("loopBack", getLoopBack())
                .append("stationName", getStationName())
                .append("stationPath", getStationPath())
                .append("remark", getRemark())
                .append("manageIp", getManageIp())
                .append("manageVlan", getManageVlan())
                .append("vlanFourThree", getVlanFourThree())
                .append("vlanFourFive", getVlanFourFive())
                .append("vlanFourSix", getVlanFourSix())
                .append("otherData", getOtherData())
                .append("upperNineThree", getUpperNineThree())
                .append("upperMes", getUpperMes())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }

    /****************************************虚拟字段*****************************************/

    private String areaName;//上级区域名称

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }


}