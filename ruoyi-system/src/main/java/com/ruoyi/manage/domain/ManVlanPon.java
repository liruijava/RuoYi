package com.ruoyi.manage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * vlan宽带PON管理对象 man_vlan_pon
 * 
 * @author LR
 * @date 2024-02-20
 */
public class ManVlanPon extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** PONid */
    private Long ponId;

    /** 设备id */
    @Excel(name = "设备id")
    private Long deviceId;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String deviceName;

    /** PON口 */
    @Excel(name = "PON口")
    private String ponName;

    /** PON资源地址 */
    @Excel(name = "PON资源地址")
    private String ponPath;

    /** 外层VLAN */
    @Excel(name = "外层VLAN")
    private String outsideVlan;

    /** 内层VLAN */
    @Excel(name = "内层VLAN")
    private String innerVlan;

    /** 划分日期 */
    @Excel(name = "划分日期")
    private String classifyDate;

    /** 槽位 */
    @Excel(name = "槽位")
    private String slot;

    /** 业务类型 */
    @Excel(name = "业务类型")
    private String businessType;

    /** 是否限光衰 */
    @Excel(name = "是否限光衰")
    private String lightFlag;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer sort;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    public void setPonId(Long ponId) 
    {
        this.ponId = ponId;
    }

    public Long getPonId() 
    {
        return ponId;
    }
    public void setDeviceId(Long deviceId) 
    {
        this.deviceId = deviceId;
    }

    public Long getDeviceId() 
    {
        return deviceId;
    }
    public void setDeviceName(String deviceName) 
    {
        this.deviceName = deviceName;
    }

    public String getDeviceName() 
    {
        return deviceName;
    }
    public void setPonName(String ponName) 
    {
        this.ponName = ponName;
    }

    public String getPonName() 
    {
        return ponName;
    }
    public void setPonPath(String ponPath) 
    {
        this.ponPath = ponPath;
    }

    public String getPonPath() 
    {
        return ponPath;
    }
    public void setOutsideVlan(String outsideVlan) 
    {
        this.outsideVlan = outsideVlan;
    }

    public String getOutsideVlan() 
    {
        return outsideVlan;
    }
    public void setInnerVlan(String innerVlan) 
    {
        this.innerVlan = innerVlan;
    }

    public String getInnerVlan() 
    {
        return innerVlan;
    }
    public void setClassifyDate(String classifyDate) 
    {
        this.classifyDate = classifyDate;
    }

    public String getClassifyDate() 
    {
        return classifyDate;
    }
    public void setSlot(String slot) 
    {
        this.slot = slot;
    }

    public String getSlot() 
    {
        return slot;
    }
    public void setSort(Integer sort) 
    {
        this.sort = sort;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getLightFlag() {
        return lightFlag;
    }

    public void setLightFlag(String lightFlag) {
        this.lightFlag = lightFlag;
    }

    public Integer getSort()
    {
        return sort;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ponId", getPonId())
            .append("deviceId", getDeviceId())
            .append("deviceName", getDeviceName())
            .append("ponName", getPonName())
            .append("ponPath", getPonPath())
            .append("outsideVlan", getOutsideVlan())
            .append("innerVlan", getInnerVlan())
            .append("classifyDate", getClassifyDate())
            .append("slot", getSlot())
            .append("lightFlag", getLightFlag())
            .append("sort", getSort())
            .append("remark", getRemark())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }

    /** 管理IP */
    @Excel(name = "管理IP")
    private String manageIp;

    /** 管理VLAN */
    @Excel(name = "管理VLAN")
    private String manageVlan;

    /** VLAN43 */
    @Excel(name = "VLAN43")
    private String vlanFourThree;

    /** VLAN45 */
    @Excel(name = "VLAN45")
    private String vlanFourFive;

    /** VLAN46 */
    @Excel(name = "VLAN46")
    private String vlanFourSix;

    /** 区域id */
    private Long areaId;

    /** 区域/设备名称 */
    @Excel(name = "区域/设备名称")
    private String areaName;

    public String getManageIp() {
        return manageIp;
    }

    public void setManageIp(String manageIp) {
        this.manageIp = manageIp;
    }

    public String getManageVlan() {
        return manageVlan;
    }

    public void setManageVlan(String manageVlan) {
        this.manageVlan = manageVlan;
    }

    public String getVlanFourThree() {
        return vlanFourThree;
    }

    public void setVlanFourThree(String vlanFourThree) {
        this.vlanFourThree = vlanFourThree;
    }

    public String getVlanFourFive() {
        return vlanFourFive;
    }

    public void setVlanFourFive(String vlanFourFive) {
        this.vlanFourFive = vlanFourFive;
    }

    public String getVlanFourSix() {
        return vlanFourSix;
    }

    public void setVlanFourSix(String vlanFourSix) {
        this.vlanFourSix = vlanFourSix;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
