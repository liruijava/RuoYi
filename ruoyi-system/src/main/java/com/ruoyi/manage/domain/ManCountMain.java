package com.ruoyi.manage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 主要业务统计数据对象 man_count_main
 * 
 * @author LR
 * @date 2024-02-22
 */
public class ManCountMain extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 地区id */
    @Excel(name = "地区id")
    private Long areaId;

    /** 地区名称 */
    @Excel(name = "地区名称")
    private String areaName;

    /** 用户数量 */
    @Excel(name = "用户数量")
    private Integer userNumber;

    /** 累计XGPON端口数 */
    @Excel(name = "累计XGPON端口数")
    private Integer ljXgponDkNumber;

    /** XGPON端口占用数 */
    @Excel(name = "XGPON端口占用数")
    private Integer xgponDkzyNumber;

    /** 真千兆用户 */
    @Excel(name = "真千兆用户")
    private Integer zqzUserNumber;

    /** 地址总数 */
    @Excel(name = "地址总数")
    private Integer addressAllNumber;

    /** XGPON地址数 */
    @Excel(name = "XGPON地址数")
    private Integer addressXgponNumber;

    /** 业务板卡数 */
    @Excel(name = "业务板卡数")
    private Integer ywbkNumber;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer sort;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    private String zqzVsXgpondk;

    private String zhanbi;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAreaId(Long areaId) 
    {
        this.areaId = areaId;
    }

    public Long getAreaId() 
    {
        return areaId;
    }
    public void setAreaName(String areaName) 
    {
        this.areaName = areaName;
    }

    public String getAreaName() 
    {
        return areaName;
    }
    public void setUserNumber(Integer userNumber) 
    {
        this.userNumber = userNumber;
    }

    public Integer getUserNumber() 
    {
        return userNumber;
    }
    public void setLjXgponDkNumber(Integer ljXgponDkNumber) 
    {
        this.ljXgponDkNumber = ljXgponDkNumber;
    }

    public Integer getLjXgponDkNumber() 
    {
        return ljXgponDkNumber;
    }
    public void setXgponDkzyNumber(Integer xgponDkzyNumber) 
    {
        this.xgponDkzyNumber = xgponDkzyNumber;
    }

    public Integer getXgponDkzyNumber() 
    {
        return xgponDkzyNumber;
    }
    public void setZqzUserNumber(Integer zqzUserNumber) 
    {
        this.zqzUserNumber = zqzUserNumber;
    }

    public Integer getZqzUserNumber() 
    {
        return zqzUserNumber;
    }
    public void setAddressAllNumber(Integer addressAllNumber) 
    {
        this.addressAllNumber = addressAllNumber;
    }

    public Integer getAddressAllNumber() 
    {
        return addressAllNumber;
    }
    public void setAddressXgponNumber(Integer addressXgponNumber) 
    {
        this.addressXgponNumber = addressXgponNumber;
    }

    public Integer getAddressXgponNumber() 
    {
        return addressXgponNumber;
    }
    public void setYwbkNumber(Integer ywbkNumber) 
    {
        this.ywbkNumber = ywbkNumber;
    }

    public Integer getYwbkNumber() 
    {
        return ywbkNumber;
    }
    public void setSort(Integer sort) 
    {
        this.sort = sort;
    }

    public Integer getSort() 
    {
        return sort;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public String getZqzVsXgpondk() {
        return zqzVsXgpondk;
    }

    public void setZqzVsXgpondk(String zqzVsXgpondk) {
        this.zqzVsXgpondk = zqzVsXgpondk;
    }

    public String getZhanbi() {
        return zhanbi;
    }

    public void setZhanbi(String zhanbi) {
        this.zhanbi = zhanbi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("areaId", getAreaId())
            .append("areaName", getAreaName())
            .append("userNumber", getUserNumber())
            .append("ljXgponDkNumber", getLjXgponDkNumber())
            .append("xgponDkzyNumber", getXgponDkzyNumber())
            .append("zqzUserNumber", getZqzUserNumber())
            .append("addressAllNumber", getAddressAllNumber())
            .append("addressXgponNumber", getAddressXgponNumber())
            .append("ywbkNumber", getYwbkNumber())
            .append("sort", getSort())
            .append("remark", getRemark())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
