package com.ruoyi.manage.domain;

public class ExcelMergeVo {

    //区域值
    private String excelValue ;

    //开始行
    private Integer startRow;

    //结束行
    private Integer endRow;

    //开始列
    private Integer startColumn;

    //结束列
    private Integer endColumn;

    public String getExcelValue() {
        return excelValue;
    }

    public void setExcelValue(String excelValue) {
        this.excelValue = excelValue;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setStartRow(Integer startRow) {
        this.startRow = startRow;
    }

    public Integer getEndRow() {
        return endRow;
    }

    public void setEndRow(Integer endRow) {
        this.endRow = endRow;
    }

    public Integer getStartColumn() {
        return startColumn;
    }

    public void setStartColumn(Integer startColumn) {
        this.startColumn = startColumn;
    }

    public Integer getEndColumn() {
        return endColumn;
    }

    public void setEndColumn(Integer endColumn) {
        this.endColumn = endColumn;
    }

    /**
     * 构造方法1
     */
    public ExcelMergeVo() {
        super();
        // TODO 自动生成的构造函数存根
    }

    /**
     * 构造方法2
     */
    public ExcelMergeVo(String excelValue, Integer startRow, Integer endRow,
                        Integer startColumn, Integer endColumn) {
        super();
        this.excelValue = excelValue;
        this.startRow = startRow;
        this.endRow = endRow;
        this.startColumn = startColumn;
        this.endColumn = endColumn;
    }

}

