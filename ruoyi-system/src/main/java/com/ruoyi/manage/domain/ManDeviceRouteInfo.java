package com.ruoyi.manage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 设备路由信息对象 man_device_route_info
 *
 * @author LR
 * @date 2024-03-27
 */
public class ManDeviceRouteInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    private Long id;

    /** 设备区县 */
    @Excel(name = "设备区县")
    private String deviceCounty;

    /** 设备位置 */
    @Excel(name = "设备位置")
    private String devicePath;

    /** 设备主键 */
    @Excel(name = "设备主键")
    private Long deviceId;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String deviceName;

    /** 设备IP */
    @Excel(name = "设备IP")
    private String deviceIp;

    /** 设备类型 */
    @Excel(name = "设备类型")
    private String deviceType;

    /** 设备实际ID */
    @Excel(name = "设备实际ID")
    private String deviceRealId;

    /** 设备端口IP */
    @Excel(name = "设备端口IP")
    private String devicePortIp;

    /** 网管名称 */
    @Excel(name = "网管名称")
    private String networkName;

    /** 光模块 */
    @Excel(name = "光模块")
    private String opticalModule;

    /** 单播外层vlan */
    @Excel(name = "单播外层vlan")
    private String outsideVlan;

    /** 单播内层vlan */
    @Excel(name = "单播内层vlan")
    private String innerVlan;

    /** BAS IP */
    @Excel(name = "BAS IP")
    private String basIp;

    /** BAS绑定端口 */
    @Excel(name = "BAS绑定端口")
    private String basPort;

    /** 电路属性 */
    @Excel(name = "电路属性")
    private String circuitProperties;

    /** 电路名称 */
    @Excel(name = "电路名称")
    private String circuitName;

    /** 聚合端口 */
    @Excel(name = "聚合端口")
    private String closePort;

    /** 物理端口 */
    @Excel(name = "物理端口")
    private String physicsPort;

    /** 链路带宽 */
    @Excel(name = "链路带宽")
    private String linkBandwidth;

    /** 对端设备主键 */
    @Excel(name = "对端设备主键")
    private Long oppositeDeviceId;

    /** 对端设备IP */
    @Excel(name = "对端设备IP")
    private String oppositeDeviceIp;

    /** 对端设备名称 */
    @Excel(name = "对端设备名称")
    private String oppositeDeviceName;

    /** 对端网管名称 */
    @Excel(name = "对端网管名称")
    private String oppositeNetworkName;

    /** 对端设备类型 */
    @Excel(name = "对端设备类型")
    private String oppositeDeviceType;

    /** 对端聚合端口 */
    @Excel(name = "对端聚合端口")
    private String opppsiteClosePort;

    /** 对端物理端口 */
    @Excel(name = "对端物理端口")
    private String oppositePhysicsPort;

    /** 对端设备实际ID */
    @Excel(name = "对端设备实际ID")
    private String oppositeDeviceRealId;

    /** 对端设备端口IP */
    @Excel(name = "对端设备端口IP")
    private String oppositeDevicePortIp;

    /** A端设备ODF位置 */
    @Excel(name = "A端设备ODF位置")
    private String deviceOdfLeft;

    /** B端设备ODF位置 */
    @Excel(name = "B端设备ODF位置")
    private String deviceOdfRight;

    /** 核心机房设备ODF */
    @Excel(name = "核心机房设备ODF")
    private String deviceOdfCore;

    /** A端波分/光缆ODF位置 */
    @Excel(name = "A端波分/光缆ODF位置")
    private String opticalOdfLeft;

    /** B端波分/光缆ODF位置 */
    @Excel(name = "B端波分/光缆ODF位置")
    private String opticalOdfRight;

    /** 核心机房波分ODF */
    @Excel(name = "核心机房波分ODF")
    private String opticalOdfCore;

    /** 核心机房波分端口 */
    @Excel(name = "核心机房波分端口")
    private String opticalPortCore;

    /** 区县机房波分ODF */
    @Excel(name = "区县机房波分ODF")
    private String opticalOdfCounty;

    /** 区县机房波分端口 */
    @Excel(name = "区县机房波分端口")
    private String opticalPortCounty;

    /** 波道信息 */
    @Excel(name = "波道信息")
    private String channelInformation;

    /** 三楼楼间缆 */
    @Excel(name = "三楼楼间缆")
    private String cableFloorThree;

    /** 二楼楼间缆 */
    @Excel(name = "二楼楼间缆")
    private String cableFloorTwo;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer sort;

    /** 是否精品 */
    @Excel(name = "是否精品")
    private String isGood;

    /** 状态（0正常 1停用） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 1代表删除） */
    private String delFlag;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setDeviceCounty(String deviceCounty)
    {
        this.deviceCounty = deviceCounty;
    }

    public String getDeviceCounty()
    {
        return deviceCounty;
    }
    public void setDevicePath(String devicePath)
    {
        this.devicePath = devicePath;
    }

    public String getDevicePath()
    {
        return devicePath;
    }
    public void setDeviceId(Long deviceId)
    {
        this.deviceId = deviceId;
    }

    public Long getDeviceId()
    {
        return deviceId;
    }
    public void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }

    public String getDeviceName()
    {
        return deviceName;
    }
    public void setDeviceIp(String deviceIp)
    {
        this.deviceIp = deviceIp;
    }

    public String getDeviceIp()
    {
        return deviceIp;
    }
    public void setDeviceType(String deviceType)
    {
        this.deviceType = deviceType;
    }

    public String getDeviceType()
    {
        return deviceType;
    }
    public void setDeviceRealId(String deviceRealId)
    {
        this.deviceRealId = deviceRealId;
    }

    public String getDeviceRealId()
    {
        return deviceRealId;
    }
    public void setDevicePortIp(String devicePortIp)
    {
        this.devicePortIp = devicePortIp;
    }

    public String getDevicePortIp()
    {
        return devicePortIp;
    }
    public void setNetworkName(String networkName)
    {
        this.networkName = networkName;
    }

    public String getNetworkName()
    {
        return networkName;
    }
    public void setOpticalModule(String opticalModule)
    {
        this.opticalModule = opticalModule;
    }

    public String getOpticalModule()
    {
        return opticalModule;
    }
    public void setOutsideVlan(String outsideVlan)
    {
        this.outsideVlan = outsideVlan;
    }

    public String getOutsideVlan()
    {
        return outsideVlan;
    }
    public void setInnerVlan(String innerVlan)
    {
        this.innerVlan = innerVlan;
    }

    public String getInnerVlan()
    {
        return innerVlan;
    }
    public void setBasIp(String basIp)
    {
        this.basIp = basIp;
    }

    public String getBasIp()
    {
        return basIp;
    }
    public void setBasPort(String basPort)
    {
        this.basPort = basPort;
    }

    public String getBasPort()
    {
        return basPort;
    }
    public void setCircuitProperties(String circuitProperties)
    {
        this.circuitProperties = circuitProperties;
    }

    public String getCircuitProperties()
    {
        return circuitProperties;
    }
    public void setCircuitName(String circuitName)
    {
        this.circuitName = circuitName;
    }

    public String getCircuitName()
    {
        return circuitName;
    }
    public void setClosePort(String closePort)
    {
        this.closePort = closePort;
    }

    public String getClosePort()
    {
        return closePort;
    }
    public void setPhysicsPort(String physicsPort)
    {
        this.physicsPort = physicsPort;
    }

    public String getPhysicsPort()
    {
        return physicsPort;
    }
    public void setLinkBandwidth(String linkBandwidth)
    {
        this.linkBandwidth = linkBandwidth;
    }

    public String getLinkBandwidth()
    {
        return linkBandwidth;
    }
    public void setOppositeDeviceId(Long oppositeDeviceId)
    {
        this.oppositeDeviceId = oppositeDeviceId;
    }

    public Long getOppositeDeviceId()
    {
        return oppositeDeviceId;
    }
    public void setOppositeDeviceIp(String oppositeDeviceIp)
    {
        this.oppositeDeviceIp = oppositeDeviceIp;
    }

    public String getOppositeDeviceIp()
    {
        return oppositeDeviceIp;
    }
    public void setOppositeDeviceName(String oppositeDeviceName)
    {
        this.oppositeDeviceName = oppositeDeviceName;
    }

    public String getOppositeDeviceName()
    {
        return oppositeDeviceName;
    }
    public void setOppositeNetworkName(String oppositeNetworkName)
    {
        this.oppositeNetworkName = oppositeNetworkName;
    }

    public String getOppositeNetworkName()
    {
        return oppositeNetworkName;
    }
    public void setOppositeDeviceType(String oppositeDeviceType)
    {
        this.oppositeDeviceType = oppositeDeviceType;
    }

    public String getOppositeDeviceType()
    {
        return oppositeDeviceType;
    }
    public void setOpppsiteClosePort(String opppsiteClosePort)
    {
        this.opppsiteClosePort = opppsiteClosePort;
    }

    public String getOpppsiteClosePort()
    {
        return opppsiteClosePort;
    }
    public void setOppositePhysicsPort(String oppositePhysicsPort)
    {
        this.oppositePhysicsPort = oppositePhysicsPort;
    }

    public String getOppositePhysicsPort()
    {
        return oppositePhysicsPort;
    }
    public void setOppositeDeviceRealId(String oppositeDeviceRealId)
    {
        this.oppositeDeviceRealId = oppositeDeviceRealId;
    }

    public String getOppositeDeviceRealId()
    {
        return oppositeDeviceRealId;
    }
    public void setOppositeDevicePortIp(String oppositeDevicePortIp)
    {
        this.oppositeDevicePortIp = oppositeDevicePortIp;
    }

    public String getOppositeDevicePortIp()
    {
        return oppositeDevicePortIp;
    }
    public void setDeviceOdfLeft(String deviceOdfLeft)
    {
        this.deviceOdfLeft = deviceOdfLeft;
    }

    public String getDeviceOdfLeft()
    {
        return deviceOdfLeft;
    }
    public void setDeviceOdfRight(String deviceOdfRight)
    {
        this.deviceOdfRight = deviceOdfRight;
    }

    public String getDeviceOdfRight()
    {
        return deviceOdfRight;
    }
    public void setDeviceOdfCore(String deviceOdfCore)
    {
        this.deviceOdfCore = deviceOdfCore;
    }

    public String getDeviceOdfCore()
    {
        return deviceOdfCore;
    }
    public void setOpticalOdfLeft(String opticalOdfLeft)
    {
        this.opticalOdfLeft = opticalOdfLeft;
    }

    public String getOpticalOdfLeft()
    {
        return opticalOdfLeft;
    }
    public void setOpticalOdfRight(String opticalOdfRight)
    {
        this.opticalOdfRight = opticalOdfRight;
    }

    public String getOpticalOdfRight()
    {
        return opticalOdfRight;
    }
    public void setOpticalOdfCore(String opticalOdfCore)
    {
        this.opticalOdfCore = opticalOdfCore;
    }

    public String getOpticalOdfCore()
    {
        return opticalOdfCore;
    }
    public void setOpticalPortCore(String opticalPortCore)
    {
        this.opticalPortCore = opticalPortCore;
    }

    public String getOpticalPortCore()
    {
        return opticalPortCore;
    }
    public void setOpticalOdfCounty(String opticalOdfCounty)
    {
        this.opticalOdfCounty = opticalOdfCounty;
    }

    public String getOpticalOdfCounty()
    {
        return opticalOdfCounty;
    }
    public void setOpticalPortCounty(String opticalPortCounty)
    {
        this.opticalPortCounty = opticalPortCounty;
    }

    public String getOpticalPortCounty()
    {
        return opticalPortCounty;
    }
    public void setChannelInformation(String channelInformation)
    {
        this.channelInformation = channelInformation;
    }

    public String getChannelInformation()
    {
        return channelInformation;
    }
    public void setCableFloorThree(String cableFloorThree)
    {
        this.cableFloorThree = cableFloorThree;
    }

    public String getCableFloorThree()
    {
        return cableFloorThree;
    }
    public void setCableFloorTwo(String cableFloorTwo)
    {
        this.cableFloorTwo = cableFloorTwo;
    }

    public String getCableFloorTwo()
    {
        return cableFloorTwo;
    }
    public void setSort(Integer sort)
    {
        this.sort = sort;
    }

    public Integer getSort()
    {
        return sort;
    }
    public void setIsGood(String isGood)
    {
        this.isGood = isGood;
    }

    public String getIsGood()
    {
        return isGood;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("deviceCounty", getDeviceCounty())
                .append("devicePath", getDevicePath())
                .append("deviceId", getDeviceId())
                .append("deviceName", getDeviceName())
                .append("deviceIp", getDeviceIp())
                .append("deviceType", getDeviceType())
                .append("deviceRealId", getDeviceRealId())
                .append("devicePortIp", getDevicePortIp())
                .append("networkName", getNetworkName())
                .append("opticalModule", getOpticalModule())
                .append("outsideVlan", getOutsideVlan())
                .append("innerVlan", getInnerVlan())
                .append("basIp", getBasIp())
                .append("basPort", getBasPort())
                .append("circuitProperties", getCircuitProperties())
                .append("circuitName", getCircuitName())
                .append("closePort", getClosePort())
                .append("physicsPort", getPhysicsPort())
                .append("linkBandwidth", getLinkBandwidth())
                .append("oppositeDeviceId", getOppositeDeviceId())
                .append("oppositeDeviceIp", getOppositeDeviceIp())
                .append("oppositeDeviceName", getOppositeDeviceName())
                .append("oppositeNetworkName", getOppositeNetworkName())
                .append("oppositeDeviceType", getOppositeDeviceType())
                .append("opppsiteClosePort", getOpppsiteClosePort())
                .append("oppositePhysicsPort", getOppositePhysicsPort())
                .append("oppositeDeviceRealId", getOppositeDeviceRealId())
                .append("oppositeDevicePortIp", getOppositeDevicePortIp())
                .append("deviceOdfLeft", getDeviceOdfLeft())
                .append("deviceOdfRight", getDeviceOdfRight())
                .append("deviceOdfCore", getDeviceOdfCore())
                .append("opticalOdfLeft", getOpticalOdfLeft())
                .append("opticalOdfRight", getOpticalOdfRight())
                .append("opticalOdfCore", getOpticalOdfCore())
                .append("opticalPortCore", getOpticalPortCore())
                .append("opticalOdfCounty", getOpticalOdfCounty())
                .append("opticalPortCounty", getOpticalPortCounty())
                .append("channelInformation", getChannelInformation())
                .append("cableFloorThree", getCableFloorThree())
                .append("cableFloorTwo", getCableFloorTwo())
                .append("sort", getSort())
                .append("remark", getRemark())
                .append("isGood", getIsGood())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}